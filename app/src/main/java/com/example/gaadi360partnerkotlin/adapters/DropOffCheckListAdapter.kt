package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.Constraints
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.models.ServicesModel

class DropOffCheckListAdapter(private val servicesModels: List<ServicesModel>?, private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ServicesModel.HEADER_TYPE -> HeaderHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false))
            ServicesModel.CHILD_TYPE -> ChildHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_service_detail, parent, false))
            else -> null
        } as RecyclerView.ViewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val `object` = servicesModels!![position]
        if (`object` != null) {
            when (`object`.value) {
                ServicesModel.HEADER_TYPE -> {
                    setAnimation((holder as HeaderHolder?)!!.itemView, position)
                    Log.d(Constraints.TAG, "HEADER_TYPE: " + `object`.serviceName)
                    holder.txtServiceName.text = `object`.serviceName
                }
                ServicesModel.CHILD_TYPE -> {
                    setAnimation((holder as ChildHolder?)!!.itemView, position)
                    Log.d(
                        Constraints.TAG,
                        "CHILD_TYPE: " + `object`.serviceItem
                    )
                    holder.ser_name_txt.text = `object`.perform + " " + `object`.serviceItem
                    holder.good_img.setOnClickListener { view: View? ->
                        holder.bad_img.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.ic_cancel
                            )
                        )
                        holder.good_img.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.ic_select
                            )
                        )
                        `object`.status = "1"
                        Log.d("CCCCC", `object`.status)
                    }
                    holder.bad_img.setOnClickListener { view: View? ->
                        holder.good_img.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.ic_deselect
                            )
                        )
                        holder.bad_img.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.ic_reject
                            )
                        )
                        `object`.status = "2"
                        Log.d("CCCCC", `object`.status)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return servicesModels?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        if (servicesModels != null) {
            val `object` = servicesModels[position]
            if (`object` != null) {
                return `object`.value
            }
        }
        return 0
    }

    class HeaderHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val txtServiceName: TextView

        init {
            txtServiceName = itemView.findViewById(R.id.txtServiceName)
        }
    }

    class ChildHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var ser_img: ImageView
        var good_img: ImageView
        var bad_img: ImageView
        var ser_name_txt: TextView

        init {
            ser_img = itemView.findViewById(R.id.ser_img)
            good_img = itemView.findViewById(R.id.good_img)
            bad_img = itemView.findViewById(R.id.bad_img)
            ser_name_txt = itemView.findViewById(R.id.ser_name_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

}