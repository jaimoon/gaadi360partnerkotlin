package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.adapters.InspAdapter.InspHolder
import com.example.gaadi360partnerkotlin.interfaces.AddonHistoryItemClickListener
import com.example.gaadi360partnerkotlin.models.InspectionModel

class InspAdapter(
    var inspectionModels: List<InspectionModel>,
    var context: BookingDetailActivity,
    private val resource: Int
) : RecyclerView.Adapter<InspHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): InspHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_insp, viewGroup, false)
        return InspHolder(view)
    }

    override fun onBindViewHolder(historyHolder: InspHolder, position: Int) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(inspectionModels[position])
    }

    override fun getItemCount(): Int {
        return inspectionModels.size
    }

    inner class InspHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var insp_img: ImageView
        var insp_txt: TextView
        var state_txt: TextView
        var addonHistoryItemClickListener: AddonHistoryItemClickListener? = null
        fun bind(inspectionModels: InspectionModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            val inspName = inspectionModels.inspectionName
            val state = inspectionModels.state
            val inspId = inspectionModels.inspectionId
            insp_txt.typeface = regular
            state_txt.typeface = bold
            insp_txt.text = inspName
            if (state.equals("0", ignoreCase = true)) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                state_txt.text = "Pending"
            }
            if (state.equals("1", ignoreCase = true)) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.green))
                state_txt.text = "Good"
            }
            if (state.equals("2", ignoreCase = true)) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.red))
                state_txt.text = "Bad"
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            addonHistoryItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            insp_img = itemView.findViewById(R.id.insp_img)
            insp_txt = itemView.findViewById(R.id.insp_txt)
            state_txt = itemView.findViewById(R.id.state_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}