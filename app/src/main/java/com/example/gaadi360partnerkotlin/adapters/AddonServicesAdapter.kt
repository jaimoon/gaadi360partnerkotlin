package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.AddonServicesActivity
import com.example.gaadi360partnerkotlin.filters.AddonServicesSearch
import com.example.gaadi360partnerkotlin.interfaces.AddonHistoryItemClickListener
import com.example.gaadi360partnerkotlin.models.AddonServicesModel
import com.example.gaadi360partnerkotlin.utils.GlobalCalls.showToast
import java.util.*

class AddonServicesAdapter(serviceIds: String, var addonServicesModels: ArrayList<AddonServicesModel>, context: AddonServicesActivity, resource: Int) : RecyclerView.Adapter<AddonServicesAdapter.AddonHolder>(), Filterable {
    var filterList: ArrayList<AddonServicesModel>
    var context: AddonServicesActivity
    var filter: AddonServicesSearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    var serviceIds: String
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): AddonHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_addon_services, viewGroup, false)
        return AddonHolder(view)
    }

    override fun onBindViewHolder(
        addonHolder: AddonHolder,
        position: Int
    ) {
        setAnimation(addonHolder.itemView, position)
        addonHolder.bind(addonServicesModels[position])
        addonHolder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {
        return addonServicesModels.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = AddonServicesSearch(filterList, this)
        }
        return filter!!
    }

    inner class AddonHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var addon_img: ImageView
        var accept_img: ImageView
        var reject_img: ImageView
        var addon_name_txt: TextView
        var addon_cost_edt: EditText
        var addonHistoryItemClickListener: AddonHistoryItemClickListener? = null
        fun bind(addonServicesModel: AddonServicesModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            addon_name_txt.typeface = regular
            addon_name_txt.text = addonServicesModel.serviceName
            addon_cost_edt.setSelection(addon_cost_edt.text.length)
            addon_cost_edt.typeface = bold
            addon_cost_edt.setText(addonServicesModel.cost)
            val myList: List<String?> =
                ArrayList(
                    Arrays.asList(
                        *serviceIds.split(",").toTypedArray()
                    )
                )
            if (myList.contains(addonServicesModel.serviceId)) {
                addon_cost_edt.isEnabled = true
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_select
                    )
                )
                reject_img.visibility = View.INVISIBLE
                addonServicesModel.status = "1"
                context.totalPrice
            }
            if (addonServicesModel.status.equals("1", ignoreCase = true)) {
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_cancel
                    )
                )
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_select
                    )
                )
            }
            if (addonServicesModel.isAddonService) {
                addon_cost_edt.isEnabled = false
            }
            addon_cost_edt.setOnTouchListener { view, motionEvent ->
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_deselect
                    )
                )
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_cancel
                    )
                )
                addonServicesModel.status = "0"
                context.totalPrice
                false
            }
            addon_cost_edt.setOnClickListener {
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_deselect
                    )
                )
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_cancel
                    )
                )
                addon_cost_edt.isFocusable = true
                addonServicesModel.status = "0"
                context.totalPrice
            }
            accept_img.setOnClickListener { view: View? ->
                if (!addon_cost_edt.text.toString().equals("", ignoreCase = true)) {
                    val editCost = addon_cost_edt.text.toString()
                    addonServicesModel.cost = editCost
                    reject_img.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_cancel
                        )
                    )
                    accept_img.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_select
                        )
                    )
                    addonServicesModel.cost = addon_cost_edt.text.toString()
                    addonServicesModel.status = "1"
                    addon_cost_edt.setSelection(addon_cost_edt.text.length)
                    addon_cost_edt.isFocusable = false
                    context.totalPrice
                    notifyDataSetChanged()
                } else {
                    showToast("Service cost not to be zero.", context)
                    addonServicesModel.cost = "0"
                    addon_cost_edt.setText("0")
                    addon_cost_edt.setSelection(addon_cost_edt.text.length)
                    context.totalPrice
                    notifyDataSetChanged()
                }
            }
            reject_img.setOnClickListener { view: View? ->
                if (!addon_cost_edt.text.toString().equals("", ignoreCase = true)) {
                    accept_img.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_deselect
                        )
                    )
                    reject_img.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_reject
                        )
                    )
                    addonServicesModel.status = "2"
                    addon_cost_edt.setSelection(addon_cost_edt.text.length)
                    addon_cost_edt.isFocusable = false
                    context.totalPrice
                    //notifyDataSetChanged();
                } else {
                    showToast("Service cost not to be zero.", context)
                    addonServicesModel.cost = "0"
                    addon_cost_edt.setText("0")
                    addon_cost_edt.setSelection(addon_cost_edt.text.length)
                    context.totalPrice
                    notifyDataSetChanged()
                }
            }
            itemView.setOnClickListener { view: View? -> }
            itemView.setOnTouchListener { v, event ->
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(addon_cost_edt.windowToken, 0)
                false
            }
        }

        override fun onClick(view: View) {
            addonHistoryItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            addon_img = itemView.findViewById(R.id.addon_img)
            accept_img = itemView.findViewById(R.id.accept_img)
            reject_img = itemView.findViewById(R.id.reject_img)
            addon_name_txt = itemView.findViewById(R.id.addon_name_txt)
            addon_cost_edt = itemView.findViewById(R.id.addon_cost_edt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = addonServicesModels
        this.context = context
        this.resource = resource
        this.serviceIds = serviceIds
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}