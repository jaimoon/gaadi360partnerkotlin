package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Matrix
import android.graphics.PointF
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.adapters.PhotosAdapter.PhotosHolder
import com.example.gaadi360partnerkotlin.interfaces.PhotosItemClickListener
import com.squareup.picasso.Picasso

class PhotosAdapter(
    var mylist: List<String>,
    var context: BookingDetailActivity,
    private val resource: Int
) : RecyclerView.Adapter<PhotosHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    var dialog: AlertDialog? = null
    var matrix = Matrix()
    var savedMatrix = Matrix()
    var mode = NONE
    var start = PointF()
    var mid = PointF()
    var oldDist = 1f
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): PhotosHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_photos, viewGroup, false)
        return PhotosHolder(view)
    }

    override fun onBindViewHolder(historyHolder: PhotosHolder, position: Int) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(mylist[position])
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    inner class PhotosHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var bike_photo_img: ImageView
        var photosItemClickListener: PhotosItemClickListener? = null
        fun bind(mylist: String) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            Picasso.with(context)
                .load(mylist)
                .placeholder(R.drawable.app_icon)
                .fit()
                .into(bike_photo_img)
            bike_photo_img.setOnClickListener { }
            bike_photo_img.setOnClickListener { zoomDialog(mylist) }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            photosItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            bike_photo_img = itemView.findViewById(R.id.bike_photo_img)
        }
    }

    private fun zoomDialog(mylist: String) {
        val builder =
            AlertDialog.Builder(context)
        builder.setCancelable(false)
        val inflater = context.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.zoom_image_dialog, null)
        val close_img =
            dialog_layout.findViewById<ImageView>(R.id.close_img)
        close_img.setOnClickListener { dialog!!.dismiss() }
        val bike_img =
            dialog_layout.findViewById<ImageView>(R.id.bike_img)
        Picasso.with(context)
            .load(mylist)
            .placeholder(R.drawable.app_icon)
            .fit()
            .into(bike_img)
        bike_img.setOnTouchListener { v, event ->
            val view = v as ImageView
            view.scaleType = ImageView.ScaleType.MATRIX
            val scale: Float
            dumpEvent(event)
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    savedMatrix.set(matrix)
                    start[event.x] = event.y
                    mode = DRAG
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> {
                    mode = NONE
                    Log.d(TAG, "mode=NONE")
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                    oldDist = spacing(event)
                    Log.d(TAG, "oldDist=$oldDist")
                    if (oldDist > 5f) {
                        savedMatrix.set(matrix)
                        midPoint(mid, event)
                        mode = ZOOM
                        Log.d(TAG, "mode=ZOOM")
                    }
                }
                MotionEvent.ACTION_MOVE -> if (mode == DRAG) {
                    matrix.set(savedMatrix)
                    matrix.postTranslate(event.x - start.x, event.y - start.y)
                } else if (mode == ZOOM) {
                    val newDist = spacing(event)
                    Log.d(TAG, "newDist=$newDist")
                    if (newDist > 5f) {
                        matrix.set(savedMatrix)
                        scale = newDist / oldDist
                        matrix.postScale(scale, scale, mid.x, mid.y)
                    }
                }
            }
            view.imageMatrix = matrix
            true
        }
        builder.setView(dialog_layout)
        dialog = builder.create()
        dialog!!.setCancelable(true)
        dialog!!.show()
    }

    private fun spacing(event: MotionEvent): Float {
        val x = event.getX(0) - event.getX(1)
        val y = event.getY(0) - event.getY(1)
        return Math.sqrt(x * x + y * y.toDouble()).toFloat()
    }

    private fun midPoint(point: PointF, event: MotionEvent) {
        val x = event.getX(0) + event.getX(1)
        val y = event.getY(0) + event.getY(1)
        point[x / 2] = y / 2
    }

    private fun dumpEvent(event: MotionEvent) {
        val names = arrayOf(
            "DOWN",
            "UP",
            "MOVE",
            "CANCEL",
            "OUTSIDE",
            "POINTER_DOWN",
            "POINTER_UP",
            "7?",
            "8?",
            "9?"
        )
        val sb = StringBuilder()
        val action = event.action
        val actionCode = action and MotionEvent.ACTION_MASK
        sb.append("event ACTION_").append(names[actionCode])
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action shr MotionEvent.ACTION_POINTER_ID_SHIFT)
            sb.append(")")
        }
        sb.append("[")
        for (i in 0 until event.pointerCount) {
            sb.append("#").append(i)
            sb.append("(pid ").append(event.getPointerId(i))
            sb.append(")=").append(event.getX(i).toInt())
            sb.append(",").append(event.getY(i).toInt())
            if (i + 1 < event.pointerCount) sb.append(";")
        }
        sb.append("]")
        Log.d("Touch Events ---------", sb.toString())
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {
        private const val TAG = "Touch"
        private const val MIN_ZOOM = 1f
        private const val MAX_ZOOM = 1f
        const val NONE = 0
        const val DRAG = 1
        const val ZOOM = 2
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}