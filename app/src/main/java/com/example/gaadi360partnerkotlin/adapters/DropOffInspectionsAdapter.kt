package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.DropOffInspectionsActivity
import com.example.gaadi360partnerkotlin.adapters.DropOffInspectionsAdapter.DropOffInspectionsHolder
import com.example.gaadi360partnerkotlin.interfaces.DropOffInspectionsItemClickListener
import com.example.gaadi360partnerkotlin.models.DropOffInspectionsModel
import java.util.*

class DropOffInspectionsAdapter(
    private val dropOffInspectionsModels: ArrayList<DropOffInspectionsModel>,
    var context: DropOffInspectionsActivity,
    private val resource: Int
) : RecyclerView.Adapter<DropOffInspectionsHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DropOffInspectionsHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_inspection, viewGroup, false)
        return DropOffInspectionsHolder(view)
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    override fun onBindViewHolder(
        inspectionHolder: DropOffInspectionsHolder,
        position: Int
    ) {
        setAnimation(inspectionHolder.itemView, position)
        inspectionHolder.bind(dropOffInspectionsModels[position])
    }

    override fun getItemCount(): Int {
        return dropOffInspectionsModels.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class DropOffInspectionsHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var insp_img: ImageView
        var accept_img: ImageView
        var reject_img: ImageView
        var insp_name_txt: TextView
        var dropOffInspectionsItemClickListener: DropOffInspectionsItemClickListener? = null

        @RequiresApi(api = Build.VERSION_CODES.N)
        fun bind(dropOffInspectionsModel: DropOffInspectionsModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            insp_name_txt.typeface = regular
            insp_name_txt.text = dropOffInspectionsModel.inspectionName
            accept_img.setOnClickListener { view: View? ->
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_cancel
                    )
                )
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_select
                    )
                )
                dropOffInspectionsModel.status = "1"
            }
            reject_img.setOnClickListener { view: View? ->
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_deselect
                    )
                )
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_reject
                    )
                )
                dropOffInspectionsModel.status = "2"
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            dropOffInspectionsItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            insp_img = itemView.findViewById(R.id.insp_img)
            accept_img = itemView.findViewById(R.id.accept_img)
            reject_img = itemView.findViewById(R.id.reject_img)
            insp_name_txt = itemView.findViewById(R.id.insp_name_txt)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}