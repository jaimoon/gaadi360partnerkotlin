package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.models.SliderModel
import com.smarteist.autoimageslider.SliderViewAdapter
import java.util.*

class SliderAdapter(private val context: Context) :
    SliderViewAdapter<SliderAdapter.SliderAdapterVH>() {
    private var mSliderModels: MutableList<SliderModel> =
        ArrayList()

    fun renewItems(sliderModels: MutableList<SliderModel>) {
        mSliderModels = sliderModels
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        mSliderModels.removeAt(position)
        notifyDataSetChanged()
    }

    fun addItem(sliderModel: SliderModel) {
        mSliderModels.add(sliderModel)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
        val inflate: View = LayoutInflater.from(parent.context).inflate(R.layout.slider_layout, null)
        return SliderAdapterVH(inflate)
    }

    override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) {
        val sliderModel = mSliderModels[position]
        viewHolder.textViewDescription.text = sliderModel.description
        viewHolder.textViewDescription.textSize = 16f
        viewHolder.textViewDescription.setTextColor(Color.WHITE)
        Glide.with(viewHolder.itemView)
            .load(sliderModel.imageUrl)
            .fitCenter()
            .into(viewHolder.imageViewBackground)
        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            //Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
        })
    }

    override fun getCount(): Int {
        //slider view count could be dynamic size
        return mSliderModels.size
    }

    inner class SliderAdapterVH(itemView: View) :
        ViewHolder(itemView) {
        var itemViewss: View
        var imageViewBackground: ImageView
        var imageGifContainer: ImageView
        var textViewDescription: TextView

        init {
            imageViewBackground =
                itemView.findViewById(R.id.iv_auto_image_slider)
            imageGifContainer =
                itemView.findViewById(R.id.iv_gif_container)
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider)
            this.itemViewss = itemView
        }
    }

}