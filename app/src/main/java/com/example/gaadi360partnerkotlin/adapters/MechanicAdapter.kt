package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.interfaces.PickUpItemClickListener
import com.example.gaadi360partnerkotlin.models.PickUpModel

class MechanicAdapter(
    var pickUpModels: List<PickUpModel>,
    var context: BookingDetailActivity,
    private val resource: Int
) : RecyclerView.Adapter<MechanicAdapter.PickUpHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): PickUpHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_pickup, viewGroup, false)
        return PickUpHolder(view)
    }

    override fun onBindViewHolder(
        pickUpHolder: PickUpHolder,
        position: Int
    ) {
        setAnimation(pickUpHolder.itemView, position)
        pickUpHolder.bind(pickUpModels[position])
    }

    override fun getItemCount(): Int {
        return pickUpModels.size
    }

    inner class PickUpHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var name_txt: TextView
        var mobile_txt: TextView
        var select_img: ImageView
        var pickUpItemClickListener: PickUpItemClickListener? = null
        fun bind(pickUpModel: PickUpModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            val name = pickUpModel.firstName
            val mobile = pickUpModel.username
            name_txt.typeface = regular
            mobile_txt.typeface = regular
            name_txt.text = name
            mobile_txt.text = mobile
            itemView.setOnClickListener { view: View? ->
                val id = pickUpModel.userId
                select_img.visibility = View.VISIBLE
                context.assignMechanic(id!!)
            }
        }

        override fun onClick(view: View) {
            pickUpItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            name_txt = itemView.findViewById(R.id.name_txt)
            mobile_txt = itemView.findViewById(R.id.mobile_txt)
            select_img = itemView.findViewById(R.id.select_img)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}