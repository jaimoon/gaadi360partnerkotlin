package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.activities.NotificationActivity
import com.example.gaadi360partnerkotlin.adapters.NotificationAdapter.NotificationHolder
import com.example.gaadi360partnerkotlin.interfaces.NotificationItemClickListener
import com.example.gaadi360partnerkotlin.models.NotificationModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class NotificationAdapter(
    var notificationModels: ArrayList<NotificationModel>,
    var context: NotificationActivity,
    private val resource: Int
) : RecyclerView.Adapter<NotificationHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    var accessToken: String? = null
    var bookingId: String? = null
    var notificationId: String? = null
    var userSessionManager: UserSessionManager? = null

    //SweetAlertDialog sweetAlertDialog;
    var dialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NotificationHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_notification, viewGroup, false)
        return NotificationHolder(view)
    }

    override fun onBindViewHolder(
        notificationHolder: NotificationHolder,
        position: Int
    ) {
        setAnimation(notificationHolder.itemView, position)
        notificationHolder.bind(notificationModels[position])
    }

    override fun getItemCount(): Int {
        return notificationModels.size
    }

    inner class NotificationHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var not_rl: RelativeLayout
        var delete_img: ImageView
        var booking_txt: TextView
        var desp_txt: TextView
        var time_txt: TextView
        var notificationItemClickListener: NotificationItemClickListener? = null
        fun bind(notificationModel: NotificationModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            userSessionManager = UserSessionManager(context)
            val userDetails =
                userSessionManager!!.userDetails
            accessToken = userDetails!![UserSessionManager.KEY_ACCSES]
            booking_txt.typeface = bold
            desp_txt.typeface = regular
            time_txt.typeface = regular
            booking_txt.text = "Booking ID : " + notificationModel.bookingId
            desp_txt.text = notificationModel.description
            time_txt.text = notificationModel.createdTime
            if (notificationModel.readFlag.equals("true", ignoreCase = true)) {
                not_rl.setBackgroundResource(R.drawable.row_border)
            } else if (notificationModel.readFlag.equals("false", ignoreCase = true)) {
                not_rl.setBackgroundResource(R.drawable.rounded_search)
            }
            delete_img.setOnClickListener {
                notificationId = notificationModel.notificationId
                showClearDialog(R.layout.warning_dialog)

                /*sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                            sweetAlertDialog.setTitleText("Are you sure?");
                            sweetAlertDialog.setContentText("You want to Clear!");
                            sweetAlertDialog.setCancelText("No, Keep It");
                            sweetAlertDialog.setConfirmText("Yes, Clear");
                            sweetAlertDialog.showCancelButton(true);

                            sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                                sweetAlertDialog.cancel();

                                notificationId = notificationModel.getNotificationId();
                                deleteNotification();

                            });

                            sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                            sweetAlertDialog.show();*/
            }
            itemView.setOnClickListener { view: View? ->
                notificationId = notificationModel.notificationId
                bookingId = notificationModel.bookingId
                readorUnread
            }
        }

        private val readorUnread: Unit
            private get() {
                val url =
                    AppUrls.BASE_URL + AppUrls.READ_UNREAD + notificationId + "/read-or-delete?flag=1"
                val stringRequest: StringRequest =
                    object : StringRequest(
                        Method.PUT,
                        url,
                        Response.Listener { response ->
                            try {
                                val jsonObject = JSONObject(response)
                                val code = jsonObject.getString("code")
                                val status = jsonObject.getString("status")
                                val message = jsonObject.getString("message")
                                val intent =
                                    Intent(context, BookingDetailActivity::class.java)
                                intent.putExtra("activity", "NotificationAdapter")
                                intent.putExtra("bookingId", bookingId)
                                context.startActivity(intent)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { error ->
                            if (error is TimeoutError || error is NoConnectionError) {
                            } else if (error is AuthFailureError) {
                            } else if (error is ServerError) {
                            } else if (error is NetworkError) {
                            } else if (error is ParseError) {
                            }
                        }
                    ) {
                        override fun getHeaders(): Map<String, String> {
                            val headers: MutableMap<String, String> =
                                HashMap()
                            headers["Authorization"] = "Bearer $accessToken"
                            return headers
                        }
                    }
                val requestQueue = Volley.newRequestQueue(context)
                requestQueue.add(stringRequest)
            }

        override fun onClick(view: View) {
            notificationItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            delete_img = itemView.findViewById(R.id.delete_img)
            not_rl = itemView.findViewById(R.id.not_rl)
            booking_txt = itemView.findViewById(R.id.booking_txt)
            desp_txt = itemView.findViewById(R.id.desp_txt)
            time_txt = itemView.findViewById(R.id.time_txt)
        }
    }

    private fun showClearDialog(layout: Int) {
        dialogBuilder = AlertDialog.Builder(context)
        val layoutView = context.layoutInflater.inflate(layout, null)
        val title_txt = layoutView.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        title_txt.text = "Are you sure?"
        val msg_txt = layoutView.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        msg_txt.text = "You want to Clear !"
        val ok_btn =
            layoutView.findViewById<Button>(R.id.ok_btn)
        ok_btn.setTypeface(bold)
        ok_btn.text = "Yes, Clear"
        val cancel_btn =
            layoutView.findViewById<Button>(R.id.cancel_btn)
        cancel_btn.setTypeface(bold)
        cancel_btn.text = "No, Keep It"
        dialogBuilder!!.setView(layoutView)
        alertDialog = dialogBuilder!!.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        alertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
        ok_btn.setOnClickListener {
            alertDialog!!.dismiss()
            deleteNotification()
        }
        cancel_btn.setOnClickListener { alertDialog!!.dismiss() }
        alertDialog!!.setCancelable(false)
    }

    private fun deleteNotification() {
        val url =
            AppUrls.BASE_URL + AppUrls.READ_UNREAD + notificationId + "/read-or-delete?flag=2"
        Log.d("URL", url)
        val stringRequest: StringRequest =
            object : StringRequest(
                Method.PUT,
                url,
                Response.Listener { response ->
                    try {
                        val jsonObject = JSONObject(response)
                        val code = jsonObject.getString("code")
                        val status = jsonObject.getString("status")
                        val message = jsonObject.getString("message")
                        context.notification
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}