package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.activities.HistoryActivity
import com.example.gaadi360partnerkotlin.adapters.HistoryAdapter.HistoryHolder
import com.example.gaadi360partnerkotlin.filters.HistorySearch
import com.example.gaadi360partnerkotlin.interfaces.HistoryItemClickListener
import com.example.gaadi360partnerkotlin.models.HistoryModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HistoryAdapter(
    var historyModels: ArrayList<HistoryModel>,
    context: HistoryActivity,
    resource: Int
) : RecyclerView.Adapter<HistoryHolder>(), Filterable {
    var filterList: ArrayList<HistoryModel>
    var context: HistoryActivity
    var filter: HistorySearch? = null
    private val li: LayoutInflater
    private val resource: Int
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): HistoryHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_history, viewGroup, false)
        return HistoryHolder(view)
    }

    override fun onBindViewHolder(historyHolder: HistoryHolder, position: Int) {
        setAnimation(historyHolder.itemView, position)
        historyHolder.bind(historyModels[position])
    }

    override fun getItemCount(): Int {
        return historyModels.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = HistorySearch(filterList, this)
        }
        return filter!!
    }

    inner class HistoryHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var bike_img: ImageView
        var status_img: ImageView
        var status_txt: TextView
        var booking_txt: TextView
        var brand_txt: TextView
        var services_txt: TextView
        var price_txt: TextView
        var last_service_date_txt: TextView
        var sst_address_txt: TextView
        var moving_txt: TextView
        var historyItemClickListener: HistoryItemClickListener? = null
        var bookingId: String? = null
        var bookingTime: String? = null
        var bookingDate: String? = null
        var status: String? = null
        var brandId: String? = null
        var modelId: String? = null
        var brandName: String? = null
        var modelName: String? = null
        var sstName: String? = null
        var sstAddress: String? = null
        fun bind(historyModel: HistoryModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            brandId = historyModel.brandId
            modelId = historyModel.brandId
            brandName = historyModel.brandName
            modelName = historyModel.modelName
            sstName = historyModel.serviceCenterName
            sstAddress = historyModel.pickupAddress
            bookingId = historyModel.bookingId
            bookingTime = historyModel.bookingTime
            bookingDate = historyModel.bookingDate
            status = historyModel.status
            if (historyModel.isMovingCondition) {
                moving_txt.setTextColor(ContextCompat.getColor(context, R.color.green))
                moving_txt.text = "Vehicle Is In Moving Condition"
            } else {
                moving_txt.setTextColor(ContextCompat.getColor(context, R.color.red))
                moving_txt.text = "Vehicle Is Not In Moving Condition"
            }
            booking_txt.typeface = bold
            booking_txt.text = "Booking Id : $bookingId"
            sst_address_txt.typeface = regular
            sst_address_txt.text = sstAddress
            brand_txt.typeface = regular
            brand_txt.text = """
                $brandName
                $modelName
                """.trimIndent()
            val services = historyModel.services
            val finalString = services!!.replace(",", "\n")
            services_txt.typeface = regular
            services_txt.text = finalString
            price_txt.typeface = bold
            price_txt.text = "\u20B9 " + historyModel.finalPrice
            last_service_date_txt.typeface = regular
            val resultDate = convertStringDateToAnotherStringDate(
                historyModel.bookingDate,
                "yyyy-MM-dd",
                "dd-MM-yyyy"
            )
            Log.d("ResultDate", resultDate)
            val df = SimpleDateFormat("HH:mm:ss")
            var d: Date? = null
            try {
                d = df.parse(bookingTime)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val cal = Calendar.getInstance()
            cal.time = d
            cal.add(Calendar.HOUR, 5)
            cal.add(Calendar.MINUTE, 30)
            var newTime = df.format(cal.time)
            try {
                val dateFormatter =
                    SimpleDateFormat("HH:mm:ss")
                val date = dateFormatter.parse(newTime)
                val timeFormatter = SimpleDateFormat("h:mm a")
                //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                newTime = timeFormatter.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            last_service_date_txt.text = "Booking Time $resultDate $newTime"
            status_txt.typeface = regular
            itemView.setOnClickListener { view: View? ->
                val intent = Intent(context, BookingDetailActivity::class.java)
                intent.putExtra("activity", "HistoryAdapter")
                intent.putExtra("bookingId", bookingId)
                context.startActivity(intent)
            }
        }

        override fun onClick(view: View) {
            historyItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            bike_img = itemView.findViewById(R.id.bike_img)
            status_img = itemView.findViewById(R.id.status_img)
            status_txt = itemView.findViewById(R.id.status_txt)
            booking_txt = itemView.findViewById(R.id.booking_txt)
            sst_address_txt = itemView.findViewById(R.id.sst_address_txt)
            brand_txt = itemView.findViewById(R.id.brand_txt)
            services_txt = itemView.findViewById(R.id.services_txt)
            price_txt = itemView.findViewById(R.id.price_txt)
            last_service_date_txt = itemView.findViewById(R.id.last_service_date_txt)
            moving_txt = itemView.findViewById(R.id.moving_txt)
        }
    }

    fun convertStringDateToAnotherStringDate(
        stringdate: String?,
        stringdateformat: String?,
        returndateformat: String?
    ): String? {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        filterList = historyModels
        this.context = context
        this.resource = resource
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}