package com.example.gaadi360partnerkotlin.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.PickUpValidateActivity
import com.example.gaadi360partnerkotlin.adapters.InspectionAdapter.InspectionHolder
import com.example.gaadi360partnerkotlin.interfaces.InspectionItemClickListener
import com.example.gaadi360partnerkotlin.models.InspectionModel
import java.util.*

class InspectionAdapter(
    var inspectionModels: ArrayList<InspectionModel>,
    var context: PickUpValidateActivity,
    private val resource: Int
) : RecyclerView.Adapter<InspectionHolder>() {
    private val li: LayoutInflater
    private var lastPosition = -1
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): InspectionHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.row_inspection, viewGroup, false)
        return InspectionHolder(view)
    }

    override fun onBindViewHolder(
        inspectionHolder: InspectionHolder,
        position: Int
    ) {
        setAnimation(inspectionHolder.itemView, position)
        inspectionHolder.bind(inspectionModels[position])
    }

    override fun getItemCount(): Int {
        return inspectionModels.size
    }

    inner class InspectionHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var insp_img: ImageView
        var insp_name_txt: TextView
        var accept_img: ImageView
        var reject_img: ImageView
        var inspectionItemClickListener: InspectionItemClickListener? = null
        fun bind(inspectionModel: InspectionModel) {
            regular = Typeface.createFromAsset(context.assets, "proxima_nova_regular.otf")
            bold = Typeface.createFromAsset(context.assets, "proxima_nova_bold.otf")
            insp_name_txt.typeface = bold
            insp_name_txt.text = inspectionModel.inspectionName
            accept_img.setOnClickListener {
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_cancel
                    )
                )
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_select
                    )
                )
                inspectionModel.state = "1"

                //context.getTotalPrice();
            }
            reject_img.setOnClickListener {
                accept_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_deselect
                    )
                )
                reject_img.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_reject
                    )
                )
                inspectionModel.state = "2"
                //context.getTotalPrice();
            }
            itemView.setOnClickListener { view: View? -> }
        }

        override fun onClick(view: View) {
            inspectionItemClickListener!!.onItemClick(view, layoutPosition)
        }

        init {
            itemView.setOnClickListener(this)
            insp_img = itemView.findViewById(R.id.insp_img)
            insp_name_txt = itemView.findViewById(R.id.insp_name_txt)
            accept_img = itemView.findViewById(R.id.accept_img)
            reject_img = itemView.findViewById(R.id.reject_img)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                android.R.anim.slide_in_left
            )
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    init {
        li =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}