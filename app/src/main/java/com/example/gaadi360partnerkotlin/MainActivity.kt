package com.example.gaadi360partnerkotlin

import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import com.example.gaadi360partnerkotlin.activities.DeliveredActivity
import com.example.gaadi360partnerkotlin.activities.HistoryActivity
import com.example.gaadi360partnerkotlin.activities.NotificationActivity
import com.example.gaadi360partnerkotlin.activities.SettingActivity
import com.example.gaadi360partnerkotlin.adapters.NewHistoryAdapter
import com.example.gaadi360partnerkotlin.models.NewHistoryModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import com.nex3z.notificationbadge.NotificationBadge
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var exit = false
    private var checkInternet = false
    var share_img: ImageView? = null
    var settings_img: ImageView? = null
    var notification_img: ImageView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var userSessionManager: UserSessionManager? = null
    var no_history_img: ImageView? = null
    var toolbar_title: TextView? = null
    var no_history_txt: TextView? = null
    var history_recyclerview: RecyclerView? = null
    var history_search: SearchView? = null
    var accessToken: String? = null
    var badge: NotificationBadge? = null
    var versionDialog: AlertDialog? = null
    var version = 0

    /*History*/
    var historyAdapter: NewHistoryAdapter? = null
    var historyModels: ArrayList<NewHistoryModel> = ArrayList<NewHistoryModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        setupBottomBar()
        checkVersionUpdate()
        share_img = findViewById(R.id.share_img)
        share_img?.setOnClickListener(this)
        settings_img = findViewById(R.id.settings_img)
        settings_img?.setOnClickListener(this)
        badge = findViewById(R.id.badge)
        notification_img = findViewById(R.id.notification_img)
        notification_img?.setOnClickListener(this)
        no_history_img = findViewById(R.id.no_history_img)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        no_history_txt = findViewById(R.id.no_history_txt)
        no_history_txt?.setTypeface(bold)
        history_recyclerview = findViewById(R.id.history_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        history_recyclerview?.setLayoutManager(layoutManager)
        historyAdapter =
            NewHistoryAdapter(historyModels, this@MainActivity, R.layout.row_new_history)
        notificationCount
        history
        history_search = findViewById(R.id.history_search)
        val searchEditText =
            history_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        history_search?.setOnClickListener(View.OnClickListener { v: View? ->
            history_search?.setIconified(false)
        })
        history_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                historyAdapter?.getFilter()?.filter(query)
                return false
            }
        })
    }

    private val notificationCount: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        try {
                            val jsonObject = JSONObject(response)
                            if (jsonObject.length() != 0) {
                                badge!!.visibility = View.VISIBLE
                                val notificationUnreadCount =
                                    jsonObject.optString("notificationUnreadCount")
                                badge!!.setNumber(notificationUnreadCount.toInt())
                            } else {
                                badge!!.visibility = View.GONE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@MainActivity)
            requestQueue.add(stringRequest)
        }

    //String url = AppUrls.BASE_URL + AppUrls.HISTORY + "1";
    private val history: Unit
        private get() {

            //String url = AppUrls.BASE_URL + AppUrls.HISTORY + "1";
            val url: String = AppUrls.BASE_URL + AppUrls.HISTORY.toString() + "4"
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        historyModels.clear()
                        try {
                            val jsonArray = JSONArray(response)
                            if (jsonArray.length() != 0) {
                                history_search!!.visibility = View.VISIBLE
                                no_history_img!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.GONE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val rm = NewHistoryModel()
                                    rm.bookingId=(jsonObject1.optString("bookingId"))
                                    rm.brandId=(jsonObject1.optString("brandId"))
                                    rm.brandName=(jsonObject1.optString("brandName"))
                                    rm.modelId=(jsonObject1.optString("modelId"))
                                    rm.modelName=(jsonObject1.optString("modelName"))
                                    rm.registrationNumber=(jsonObject1.optString("registrationNumber"))
                                    rm.bookingDate=(jsonObject1.optString("bookingDate"))
                                    rm.bookingTime=(jsonObject1.optString("bookingTime"))
                                    rm.finalPrice=(jsonObject1.optString("finalPrice"))
                                    rm.pickupAddress=(jsonObject1.optString("pickupAddress"))
                                    rm.status=(jsonObject1.optString("status"))
                                    rm.serviceCenterName=(jsonObject1.optString("serviceCenterName"))
                                    rm.paymentStatus=(jsonObject1.optString("paymentStatus"))
                                    rm.promocodeAmount=(jsonObject1.optString("promocodeAmount"))
                                    rm.isMovingCondition=(jsonObject1.optBoolean("movingCondition"))
                                    var services: String? = ""
                                    if (jsonObject1.has("services")) {
                                        val servicesArray =
                                            jsonObject1.getJSONArray("services")
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            services = IntStream.range(
                                                0,
                                                servicesArray.length()
                                            ).mapToObj { j: Int ->
                                                try {
                                                    return@mapToObj servicesArray.getJSONObject(
                                                        j
                                                    ).getString("serviceName")
                                                } catch (e: JSONException) {
                                                    e.printStackTrace()
                                                    return@mapToObj ""
                                                }
                                            }.collect(Collectors.joining("\n"))
                                        }
                                        rm.services=(services)
                                    }
                                    historyModels.add(rm)
                                }
                                history_recyclerview!!.adapter = historyAdapter
                                historyAdapter?.notifyDataSetChanged()
                            } else {
                                history_search!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@MainActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === share_img) {
            if (checkInternet) {
                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Gaadi360")
                sharingIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    """
                        A two-wheeler service that's sure to give you more SMILEAGE.
                        Enjoy unparalleled service with Gaadi360.  
                        Download at app.gaadi360.com
                        """.trimIndent()
                )
                startActivity(Intent.createChooser(sharingIntent, "Share via"))
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === settings_img) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, SettingActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === notification_img) {
            if (checkInternet) {
                val intent = Intent(this@MainActivity, NotificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun setupBottomBar() {
        val bottomNavigationBar =
            findViewById<BottomNavigationBar>(R.id.bottom_bar)
        val home =
            BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.newHistory)
        val history = BottomBarItem(R.drawable.ic_diploma, R.string.inProcessHistory)
        val settings =
            BottomBarItem(R.drawable.ic_diploma, R.string.deliveredHistory)
        bottomNavigationBar
            .addTab(home)
            .addTab(history)
            .addTab(settings)
        bottomNavigationBar.selectTab(0, true)
        bottomNavigationBar.setOnSelectListener { position -> showContent(position) }
    }

    fun showContent(position: Int) {
        if (position == 1) {
            val intent = Intent(this@MainActivity, HistoryActivity::class.java)
            startActivity(intent)
        }
        if (position == 2) {
            val intent = Intent(this@MainActivity, DeliveredActivity::class.java)
            startActivity(intent)
        }
    }

    private fun checkVersionUpdate() {
        try {
            val pInfo =
                this@MainActivity.packageManager.getPackageInfo(packageName, 0)
            version = pInfo.versionCode
            Log.d("VERSION", "" + version)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val url: String = AppUrls.BASE_URL + AppUrls.VERSION_UPDATE
        val stringRequest = StringRequest(
            Request.Method.GET,
            url,
            Response.Listener { response ->
                try {
                    val jsonObject = JSONObject(response)
                    val id = jsonObject.optString("id")
                    val platformId = jsonObject.optString("platformId")
                    val currentVersionCode = jsonObject.optInt("currentVersionCode")
                    val mandatoryUpdateVersionCode =
                        jsonObject.optInt("mandatoryUpdateVersionCode")
                    val versionName = jsonObject.optString("versionName")
                    val mandatory = jsonObject.optString("mandatory")
                    val status = jsonObject.optString("status")
                    val createdTime = jsonObject.optString("createdTime")
                    val modifiedTime = jsonObject.optString("modifiedTime")
                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert()
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert()
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error: VolleyError? ->
                if (error is TimeoutError || error is NoConnectionError) {
                    GlobalCalls.showToast("Connection Not Found..!", this@MainActivity)
                } else if (error is AuthFailureError) {
                    GlobalCalls.showToast("User Not Found..!", this@MainActivity)
                } else if (error is ServerError) {
                    GlobalCalls.showToast("Server Not Found..!", this@MainActivity)
                } else if (error is NetworkError) {
                    GlobalCalls.showToast("Network Not Found..!", this@MainActivity)
                } else if (error is ParseError) {
                    GlobalCalls.showToast("Please Try Later..!", this@MainActivity)
                }
            }
        )
        val requestQueue = Volley.newRequestQueue(this@MainActivity)
        requestQueue.add(stringRequest)
    }

    fun versionUpdateAlert() {
        val builder =
            AlertDialog.Builder(this@MainActivity)
        val inflater = this@MainActivity.layoutInflater
        val dialog_layout =
            inflater.inflate(R.layout.version_update_dialog, null)
        val title_txt = dialog_layout.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        val msg_txt = dialog_layout.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        val update_btn =
            dialog_layout.findViewById<Button>(R.id.update_btn)
        update_btn.setTypeface(bold)
        update_btn.setOnClickListener {
            if (checkInternet) {
                val appPackageName = packageName
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        builder.setView(dialog_layout)
        versionDialog = builder.create()
        versionDialog!!.setCancelable(false)
        versionDialog!!.show()
    }

    fun optionalVersionUpdateAlert() {
        val builder =
            AlertDialog.Builder(this@MainActivity)
        val inflater = this@MainActivity.layoutInflater
        val dialog_layout =
            inflater.inflate(R.layout.version_update_dialog, null)
        val title_txt = dialog_layout.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        val msg_txt = dialog_layout.findViewById<TextView>(R.id.msg_txt)
        msg_txt.setTypeface(regular)
        val update_btn =
            dialog_layout.findViewById<Button>(R.id.update_btn)
        update_btn.setTypeface(bold)
        update_btn.setOnClickListener {
            if (checkInternet) {
                val appPackageName = packageName
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        builder.setView(dialog_layout).setNegativeButton(
            "CANCEL"
        ) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        versionDialog = builder.create()
        versionDialog!!.setCancelable(false)
        versionDialog!!.show()
    }

    override fun onBackPressed() {
        if (exit) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            moveTaskToBack(true)
        } else {
            GlobalCalls.showToast("Press Back again to Exit.!", this@MainActivity)
            exit = true
            Handler().postDelayed({ exit = false }, 2 * 1000.toLong())
        }
    }
}

