package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface DropOffCheckListItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}