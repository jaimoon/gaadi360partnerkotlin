package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface DropOffInspectionsItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}