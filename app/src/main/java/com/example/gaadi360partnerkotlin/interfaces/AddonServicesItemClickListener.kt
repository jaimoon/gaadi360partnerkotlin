package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface AddonServicesItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}