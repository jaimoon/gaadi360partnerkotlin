package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface DeliveredHistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}