package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface NotificationItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}