package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface PickUpItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}