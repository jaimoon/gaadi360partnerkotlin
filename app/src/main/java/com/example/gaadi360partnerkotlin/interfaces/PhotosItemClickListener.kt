package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface PhotosItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}