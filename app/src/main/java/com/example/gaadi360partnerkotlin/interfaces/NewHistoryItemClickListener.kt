package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface NewHistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}