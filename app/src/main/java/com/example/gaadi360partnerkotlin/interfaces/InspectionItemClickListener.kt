package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface InspectionItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}