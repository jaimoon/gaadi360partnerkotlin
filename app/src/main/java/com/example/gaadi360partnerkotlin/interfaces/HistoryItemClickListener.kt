package com.example.gaadi360partnerkotlin.interfaces

import android.view.View

interface HistoryItemClickListener {
    fun onItemClick(v: View?, pos: Int)
}