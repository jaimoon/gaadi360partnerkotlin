package com.example.gaadi360partnerkotlin.utils

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.widget.TextView
import android.widget.Toast
import com.example.gaadi360partnerkotlin.R
import com.taishi.flipprogressdialog.FlipProgressDialog
import java.util.*

object GlobalCalls {
    fun showToast(msg: String?, ctx: Context?) {

        val text: TextView

        val toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG)
        val view = toast.view
        view.background.setColorFilter(Color.parseColor("#FF8000"), PorterDuff.Mode.SRC_IN)
        text = view.findViewById(android.R.id.message);
        text.setTextColor(Color.parseColor("#FFFFFF"))
        toast.show()
    }

    fun showProgressDialog() {
        val imageList: MutableList<Int?> = ArrayList()
        val flipProgressDialog: FlipProgressDialog?
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog.setImageList(imageList)
        flipProgressDialog.setCanceledOnTouchOutside(true)
        flipProgressDialog.setDimAmount(0.8f) //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog.setBackgroundAlpha(0.2f)
        flipProgressDialog.setBorderStroke(0)
        flipProgressDialog.setBorderColor(-1)
        flipProgressDialog.setCornerRadius(16)
        flipProgressDialog.setImageSize(200)
        flipProgressDialog.setImageMargin(10)
        flipProgressDialog.setOrientation("rotationY")
        flipProgressDialog.setDuration(600)
        flipProgressDialog.setStartAngle(0.0f)
        flipProgressDialog.setEndAngle(180.0f)
        flipProgressDialog.setMinAlpha(0.0f)
        flipProgressDialog.setMaxAlpha(1.0f)

        //flipProgressDialog.show(getFragmentManager(), "");
    }

    fun cancelProgressDialog() {
        val imageList: MutableList<Int?> = ArrayList()
        val flipProgressDialog: FlipProgressDialog?
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog.setImageList(imageList)
        flipProgressDialog.setCanceledOnTouchOutside(true)
        flipProgressDialog.setDimAmount(0.8f) //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog.setBackgroundAlpha(0.2f)
        flipProgressDialog.setBorderStroke(0)
        flipProgressDialog.setBorderColor(-1)
        flipProgressDialog.setCornerRadius(16)
        flipProgressDialog.setImageSize(200)
        flipProgressDialog.setImageMargin(10)
        flipProgressDialog.setOrientation("rotationY")
        flipProgressDialog.setDuration(600)
        flipProgressDialog.setStartAngle(0.0f)
        flipProgressDialog.setEndAngle(180.0f)
        flipProgressDialog.setMinAlpha(0.0f)
        flipProgressDialog.setMaxAlpha(1.0f)
        flipProgressDialog.dismiss()
    }
}