package com.example.gaadi360partnerkotlin.utils

object AppUrls {
    //public static String BASE_URL = "https://admin.gaadi360.com/gaadi2/api/";
    var BASE_URL: String? = "http://test.gaadi360.com/gaadi/api/"

    //public static String BASE_URL = "http://192.168.32.97:8080/gaadi/api/";
    var IMAGE_URL: String? = BASE_URL + "media/file/display/"
    var SERVICETYPE: String? = "service/type"
    var VERSION_UPDATE: String? = "build/info/1/2"
    var PROFILE: String? = "secure/profile/get"
    var EDIT_PROFILE: String? = "secure/profile/update"
    var EDIT_PROFILE_PIC: String? = "media/file/upload"
    var HELP_CENTER: String? = "secure/help-center/get"
    var LOGIN: String? = "login/generate/otp?mobile="
    var VERIFYOTP: String? = "login/via/otp?mobile="
    var FCM: String? = "secure/user/device"
    var HISTORY: String? = "secure/V2/service_center/bookings?status="
    var HISTORY_DETAIL: String? = "secure/booking/"
    var NOTIFICATIONS: String? = "secure/notifications"
    var READ_UNREAD: String? = "secure/notification/"
    var CLEAR_NOTIFICATIONS: String? = "secure/notification/all/clear"
    var NOTIFICATION_COUNT: String? = "secure/notifications/unread-count"
    var ASSIGN_PICKUP: String? = "secure/service_center/pickup_persons"
    var ASSIGNED_PICKUP: String? = "/assign/pickup_person?pickupUserId="
    var PICKUP_OTP: String? = "secure/generate_otp?bookingId="
    var PICKUP_VERIFY_OTP: String? = "secure/V2/verify/otp?otp="
    var SEND_PICKUP: String? = "secure/booking/pickup"
    var SEND_MECHANIC: String? = "secure/service_center/mechanics"
    var ASSIGN_MECHANIC: String? = "/assign/mechanic?mechanicUserId="
    var ADDON_SERVICES: String? = "secure/service-center/"
    var SEND_ADDON: String? = "secure/booking/services/add"
    var SERVICE_COMPLETED: String? = "secure/v2/booking/service_completed"
    var SEND_BIKE_IMAGE: String? = "media/file/upload"
    var DROP_OFF_INSPECTIONS: String? = "V2/inspections"
    var DROP_OFF: String? = "secure/booking/"
    var DELIVERED: String? = "secure/booking/"
    var CALL_OTP: String? = "V2/login/resend/otp?mobile="
}