package com.example.gaadi360partnerkotlin.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.gaadi360partnerkotlin.MainActivity
import com.example.gaadi360partnerkotlin.activities.LoginActivity
import java.util.*

class UserSessionManager @SuppressLint("CommitPrefEdits") constructor(private val _context: Context?) {
    private val sharedPreferences: SharedPreferences?
    private val editor: SharedPreferences.Editor?
    fun createDeviceId(deviceId: String?) {
        editor!!.putString(DEVICE_ID, deviceId)
        editor.apply()
    }

    fun checkLogin(): Boolean {
        if (isUserLoggedIn) {
            val i = Intent(_context, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            _context!!.startActivity(i)
            return true
        }
        return false
    }

    fun createUserLoginSession(
        firstName: String?,
        username: String?,
        email: String?,
        mobile: String?,
        accessToken: String?,
        mediaSecret: String?
    ) {
        editor!!.putBoolean(IS_USER_LOGIN, true)
        //editor.putInt(USER_ID, userId);
        editor.putString(FIRST_NAME, firstName)
        editor.putString(USER_NAME, username)
        editor.putString(USER_EMAIL, email)
        editor.putString(USER_MOBILE, mobile)
        editor.putString(KEY_ACCSES, accessToken)
        editor.putString(MEDIA_SECRET, mediaSecret)
        editor.apply()
    }

    //user.put(USER_ID, sharedPreferences.getInt(USER_ID, null));
    val userDetails: HashMap<String?, String?>?
        get() {
            val user =
                HashMap<String?, String?>()
            user[KEY_ACCSES] = sharedPreferences!!.getString(KEY_ACCSES, null)
            //user.put(USER_ID, sharedPreferences.getInt(USER_ID, null));
            user[USER_NAME] = sharedPreferences.getString(USER_NAME, null)
            user[USER_MOBILE] = sharedPreferences.getString(USER_MOBILE, null)
            user[USER_EMAIL] = sharedPreferences.getString(USER_EMAIL, null)
            user[DEVICE_ID] = sharedPreferences.getString(DEVICE_ID, null)
            return user
        }

    val isUserLoggedIn: Boolean
        get() = !sharedPreferences!!.getBoolean(IS_USER_LOGIN, false)

    fun logoutUser() {
        editor!!.clear()
        editor.apply()
        val i = Intent(_context, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        _context!!.startActivity(i)
    }

    companion object {
        private val PREFER_NAME: String? = "Gaadi360Partner"
        private val IS_USER_LOGIN: String? = "IsUserLoggedIn"
        val INTRO_SLIDE: String? = "intorslide"
        val ISFIRTTIMEAPP: String? = "isfirsttime"
        val DEVICE_ID: String? = "device_id"
        val KEY_ACCSES: String? = "access_key"
        val USER_ID: Int? = 100
        val FIRST_NAME: String? = "firstName"
        val USER_NAME: String? = "user_name"
        val USER_MOBILE: String? = "user_mobile"
        val USER_EMAIL: String? = "user_email"
        val MEDIA_SECRET: String? = "mediaSecret"
    }

    init {
        val PRIVATE_MODE = 0
        sharedPreferences =
            _context!!.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = sharedPreferences.edit()
    }
}