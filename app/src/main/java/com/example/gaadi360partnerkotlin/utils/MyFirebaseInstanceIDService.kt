package com.example.gaadi360partnerkotlin.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.SplashScreenActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseInstanceIDService : FirebaseMessagingService() {

    var pendingIntent: PendingIntent? = null
    var builder: NotificationCompat.Builder? = null
    var NOTIFICATION_ID = 2020
    var notificationManager: NotificationManager? = null
    var pattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d("NEW_TOKEN", s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        val message = remoteMessage.data["message"]
        val imageUri = remoteMessage.data["image"]

        Log.d("NotCheck", "Remote\n$message\n$imageUri")
        Log.d("Notification", """${remoteMessage.notification!!.title}""".trimIndent() + remoteMessage.notification?.getBody())

        notificationManager = this@MyFirebaseInstanceIDService.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this@MyFirebaseInstanceIDService, SplashScreenActivity::class.java)
        pendingIntent = PendingIntent.getActivity(this@MyFirebaseInstanceIDService, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val icon = BitmapFactory.decodeResource(this@MyFirebaseInstanceIDService.resources, R.drawable.app_icon)
        builder = NotificationCompat.Builder(this@MyFirebaseInstanceIDService, "default")
            .setSmallIcon(R.drawable.app_icon)
            .setContentTitle(remoteMessage.notification!!.title)
            .setContentText(remoteMessage.notification!!.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setLargeIcon(icon)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setContentIntent(pendingIntent)

        if (remoteMessage.notification != null) {
            Log.d("Notification", remoteMessage.notification!!.body)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    "com.example.gaadi360partnerkotlin",
                    "NOTIFICATION_CHANNEL_NAME",
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.enableLights(true)
                channel.lightColor = Color.RED
                channel.enableVibration(true)
                channel.vibrationPattern = pattern
                builder?.setChannelId("com.example.gaadi360partnerkotlin")
                if (notificationManager != null) {
                    notificationManager!!.createNotificationChannel(channel)
                }
            }
            assert(notificationManager != null)
            notificationManager!!.notify(NOTIFICATION_ID, builder?.build())
        }
    }
}