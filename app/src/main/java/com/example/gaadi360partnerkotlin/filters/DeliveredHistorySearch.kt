package com.example.gaadi360partnerkotlin.filters

import android.widget.Filter
import com.example.gaadi360partnerkotlin.adapters.DeliveredHistoryAdapter
import com.example.gaadi360partnerkotlin.models.DeliveredHistoryModel
import java.util.*

class DeliveredHistorySearch(
    var filterList: ArrayList<DeliveredHistoryModel>,
    var adapter: DeliveredHistoryAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<DeliveredHistoryModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceCenterName!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingId!!.toUpperCase().contains(constraint) ||
                    filterList[i].brandName!!.toUpperCase().contains(constraint) ||
                    filterList[i].modelName!!.toUpperCase().contains(constraint) ||
                    filterList[i].services!!.toUpperCase().contains(constraint) ||
                    filterList[i].finalPrice!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingDate!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingTime!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.historyModels = results.values as ArrayList<DeliveredHistoryModel>
        adapter.notifyDataSetChanged()
    }

}