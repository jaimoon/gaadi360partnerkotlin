package com.example.gaadi360partnerkotlin.filters

import android.widget.Filter
import com.example.gaadi360partnerkotlin.adapters.NewHistoryAdapter
import com.example.gaadi360partnerkotlin.models.NewHistoryModel
import java.util.*

class NewHistorySearch(
    var filterList: ArrayList<NewHistoryModel>,
    var adapter: NewHistoryAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<NewHistoryModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceCenterName!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingId!!.toUpperCase().contains(constraint) ||
                    filterList[i].brandName!!.toUpperCase().contains(constraint) ||
                    filterList[i].modelName!!.toUpperCase().contains(constraint) ||
                    filterList[i].services!!.toUpperCase().contains(constraint) ||
                    filterList[i].finalPrice!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingDate!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingTime!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.historyModels = results.values as ArrayList<NewHistoryModel>
        adapter.notifyDataSetChanged()
    }

}