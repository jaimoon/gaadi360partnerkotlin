package com.example.gaadi360partnerkotlin.filters

import android.widget.Filter
import com.example.gaadi360partnerkotlin.adapters.HistoryAdapter
import com.example.gaadi360partnerkotlin.models.HistoryModel
import java.util.*

class HistorySearch(
    var filterList: ArrayList<HistoryModel>,
    var adapter: HistoryAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<HistoryModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceCenterName!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingId!!.toUpperCase().contains(constraint) ||
                    filterList[i].brandName!!.toUpperCase().contains(constraint) ||
                    filterList[i].modelName!!.toUpperCase().contains(constraint) ||
                    filterList[i].services!!.toUpperCase().contains(constraint) ||
                    filterList[i].finalPrice!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingDate!!.toUpperCase().contains(constraint) ||
                    filterList[i].bookingTime!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.historyModels = results.values as ArrayList<HistoryModel>
        adapter.notifyDataSetChanged()
    }

}