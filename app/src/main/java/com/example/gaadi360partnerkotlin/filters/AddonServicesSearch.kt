package com.example.gaadi360partnerkotlin.filters

import android.widget.Filter
import com.example.gaadi360partnerkotlin.adapters.AddonServicesAdapter
import com.example.gaadi360partnerkotlin.models.AddonServicesModel
import java.util.*

class AddonServicesSearch(
    var filterList: ArrayList<AddonServicesModel>,
    var adapter: AddonServicesAdapter
) : Filter() {
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredPlayers =
                ArrayList<AddonServicesModel>()
            for (i in filterList.indices) {
                if (filterList[i].serviceId!!.toUpperCase().contains(constraint) ||
                    filterList[i].serviceName!!.toUpperCase().contains(constraint) ||
                    filterList[i].cost!!.toUpperCase().contains(constraint) ||
                    filterList[i].createdTime!!.toUpperCase().contains(constraint) ||
                    filterList[i].modifiedTime!!.toUpperCase().contains(constraint)
                ) {
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        adapter.addonServicesModels = results.values as ArrayList<AddonServicesModel>
        adapter.notifyDataSetChanged()
    }

}