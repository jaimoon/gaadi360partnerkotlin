package com.example.gaadi360partnerkotlin.models

class NewHistoryModel {
    var bookingId: String? = null
    var brandId: String? = null
    var brandName: String? = null
    var modelId: String? = null
    var modelName: String? = null
    var registrationNumber: String? = null
    var bookingDate: String? = null
    var bookingTime: String? = null
    var finalPrice: String? = null
    var services: String? = null
    var pickupAddress: String? = null
    var status: String? = null
    var serviceCenterName: String? = null
    var paymentStatus: String? = null
    var promocodeAmount: String? = null
    var paidAmount: String? = null
    var isMovingCondition = false

}