package com.example.gaadi360partnerkotlin.models

class PickUpModel {
    var userId: String? = null
    var firstName: String? = null
    var username: String? = null
    var locked: String? = null
    var enabled: String? = null
    var expired: String? = null
    var authority: String? = null
    var email: String? = null
    var mobile: String? = null
    var emailVerified: String? = null
    var mobileVerified: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null

}