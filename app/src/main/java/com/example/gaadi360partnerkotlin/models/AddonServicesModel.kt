package com.example.gaadi360partnerkotlin.models

class AddonServicesModel {
    var serviceId: String? = null
    var serviceName: String? = null
    var deleted: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null
    var cost: String? = null
    var status = "0"
    var isAddonService = false

}