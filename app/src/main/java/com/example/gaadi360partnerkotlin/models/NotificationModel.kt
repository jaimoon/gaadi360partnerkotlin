package com.example.gaadi360partnerkotlin.models

class NotificationModel {
    var notificationId: String? = null
    var userId: String? = null
    var bookingId: String? = null
    var description: String? = null
    var deleted: String? = null
    var readFlag: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null

}