package com.example.gaadi360partnerkotlin.models

class DropOffServicesModel {
    var serviceId: String? = null
    var serviceName: String? = null
    var bookingId: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null
    var cost: String? = null
    var type: String? = null
    var serviceDetailModels: List<ServiceDetailModel>? = null

}