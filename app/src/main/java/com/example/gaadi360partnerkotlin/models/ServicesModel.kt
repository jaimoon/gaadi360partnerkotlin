package com.example.gaadi360partnerkotlin.models

class ServicesModel(
    var serviceId: String,
    var serviceName: String,
    var createdTime: String,
    var modifiedTime: String,
    var bookingId: String,
    var cost: String,
    var type: String,
    var perform: String,
    var serviceItem: String,
    var status: String,
    var value: Int
) {

    companion object {
        const val HEADER_TYPE = 0
        const val CHILD_TYPE = 1
    }

}