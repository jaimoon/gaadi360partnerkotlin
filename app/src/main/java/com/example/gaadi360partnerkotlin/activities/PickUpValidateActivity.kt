package com.example.gaadi360partnerkotlin.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.MainActivity
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.adapters.InspectionAdapter
import com.example.gaadi360partnerkotlin.models.InspectionModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls.showToast
import com.example.gaadi360partnerkotlin.utils.NetworkChecking.isConnected
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.example.gaadi360partnerkotlin.utils.VolleyMultipartRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*

class PickUpValidateActivity : AppCompatActivity(),
    View.OnClickListener {
    private var checkInternet = false
    var userSessionManager: UserSessionManager? = null
    var toolbar_title: TextView? = null
    var insp_txt: TextView? = null
    var photo_txt: TextView? = null
    var reg_edt: EditText? = null
    var remarks_edt: EditText? = null
    var close: ImageView? = null
    var bike_img_one: ImageView? = null
    var bike_img_two: ImageView? = null
    var bike_img_three: ImageView? = null
    var bike_img_four: ImageView? = null
    var bike_img_five: ImageView? = null
    var bike_img_six: ImageView? = null
    var submit_btn: Button? = null
    var insp_recyclerview: RecyclerView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var accessToken: String? = null
    var bookingId: String? = null
    var inspectionAdapter: InspectionAdapter? = null
    var inspectionModels =
        ArrayList<InspectionModel>()
    var list = ArrayList<String?>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_up_validate)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails =
            userSessionManager!!.userDetails
        accessToken = userDetails!![UserSessionManager.KEY_ACCSES]
        bookingId = intent.getStringExtra("bookingId")
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        insp_txt = findViewById(R.id.insp_txt)
        insp_txt?.setTypeface(bold)
        photo_txt = findViewById(R.id.photo_txt)
        photo_txt?.setTypeface(bold)
        reg_edt = findViewById(R.id.reg_edt)
        reg_edt?.setTypeface(regular)
        remarks_edt = findViewById(R.id.remarks_edt)
        remarks_edt?.setTypeface(regular)
        bike_img_one = findViewById(R.id.bike_img_one)
        bike_img_one?.setOnClickListener(this)
        bike_img_two = findViewById(R.id.bike_img_two)
        bike_img_two?.setOnClickListener(this)
        bike_img_three = findViewById(R.id.bike_img_three)
        bike_img_three?.setOnClickListener(this)
        bike_img_four = findViewById(R.id.bike_img_four)
        bike_img_four?.setOnClickListener(this)
        bike_img_five = findViewById(R.id.bike_img_five)
        bike_img_five?.setOnClickListener(this)
        bike_img_six = findViewById(R.id.bike_img_six)
        bike_img_six?.setOnClickListener(this)
        submit_btn = findViewById(R.id.submit_btn)
        submit_btn?.setOnClickListener(this)
        insp_recyclerview = findViewById(R.id.insp_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        insp_recyclerview?.setLayoutManager(layoutManager)
        inspectionAdapter = InspectionAdapter(
            inspectionModels,
            this@PickUpValidateActivity,
            R.layout.row_inspection
        )
        inspections
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 101)
            }
        }
    }

    private val inspections: Unit
        private get() {
            val url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail"
            Log.d("URL", url)
            val stringRequest: StringRequest = object : StringRequest(
                Method.GET,
                url,
                Response.Listener { response ->
                    Log.d("RESP", response)
                    try {
                        val jsonObject = JSONObject(response)
                        val bookingId = jsonObject.optString("bookingId")
                        val userId = jsonObject.optString("userId")
                        val orderId = jsonObject.optString("orderId")
                        val serviceCenterId = jsonObject.optString("serviceCenterId")
                        val bookingType = jsonObject.optString("bookingType")
                        val brandId = jsonObject.optString("brandId")
                        val brandName = jsonObject.optString("brandName")
                        val modelId = jsonObject.optString("modelId")
                        val modelName = jsonObject.optString("modelName")
                        val kilometerRangeId =
                            jsonObject.optString("kilometerRangeId")
                        val bookingDate = jsonObject.optString("bookingDate")
                        val bookingTime = jsonObject.optString("bookingTime")
                        val pickupAddress = jsonObject.optString("pickupAddress")
                        val pickupAddressLatitude =
                            jsonObject.optString("pickupAddressLatitude")
                        val pickupAddressLongitude =
                            jsonObject.optString("pickupAddressLongitude")
                        val pickedUpTime = jsonObject.optString("pickedUpTime")
                        val deliveredTime = jsonObject.optString("deliveredTime")
                        val addressType = jsonObject.optString("addressType")
                        val estimatedCost = jsonObject.optString("estimatedCost")
                        val finalPrice = jsonObject.optString("finalPrice")
                        val promocodeAmount = jsonObject.optString("promocodeAmount")
                        val initialPaidAmount =
                            jsonObject.optString("initialPaidAmount")
                        val paymentStatus = jsonObject.optString("paymentStatus")
                        val status = jsonObject.optString("status")
                        val deleted = jsonObject.optString("deleted")
                        val createdTime = jsonObject.optString("createdTime")
                        val modifiedTime = jsonObject.optString("modifiedTime")
                        val firstName = jsonObject.optString("firstName")
                        val lastName = jsonObject.optString("lastName")
                        val serviceCenterName =
                            jsonObject.optString("serviceCenterName")
                        val contactMobileNumber =
                            jsonObject.optString("contactMobileNumber")
                        val mobile = jsonObject.optString("mobile")
                        val rescheduleCount = jsonObject.optString("rescheduleCount")
                        val paidAmount = jsonObject.optString("paidAmount")
                        val addonsAmount = jsonObject.optString("addonsAmount")
                        if (jsonObject.has("inspections")) {
                            insp_txt!!.visibility = View.VISIBLE
                            insp_recyclerview!!.visibility = View.VISIBLE
                            val jsonArray = jsonObject.getJSONArray("inspections")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val im = InspectionModel()
                                im.bookingId = jsonObject1.getString("bookingId")
                                im.inspectionId = jsonObject1.getString("inspectionId")
                                im.state = jsonObject1.getString("state")
                                im.inspectionName = jsonObject1.getString("inspectionName")
                                inspectionModels.add(im)
                            }
                            insp_recyclerview!!.adapter = inspectionAdapter
                            inspectionAdapter!!.notifyDataSetChanged()
                        } else {
                            insp_txt!!.visibility = View.GONE
                            insp_recyclerview!!.visibility = View.GONE
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError? ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
            val requestQueue = Volley.newRequestQueue(this@PickUpValidateActivity)
            requestQueue.add(stringRequest)
        }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onClick(v: View) {
        if (v === close) {
            val intent = Intent(this@PickUpValidateActivity, MainActivity::class.java)
            startActivity(intent)
        }
        if (v === bike_img_one) {

            /*From Gallery*/
            /*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 100);*/

            /*From Camera*/
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 101)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 101)
            }
        }
        if (v === bike_img_two) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 102)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 102)
            }
        }
        if (v === bike_img_three) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 103)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 103)
            }
        }
        if (v === bike_img_four) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 104)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 104)
            }
        }
        if (v === bike_img_five) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 105)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 105)
            }
        }
        if (v === bike_img_six) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 106)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 106)
            }
        }
        if (v === submit_btn) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val status = inspectionModels.stream()
                    .anyMatch { t: InspectionModel ->
                        t.state.equals(
                            "0",
                            ignoreCase = true
                        )
                    }
                if (!status) {
                    if (reg_edt!!.text.toString().isEmpty()) {
                        reg_edt!!.error = "Please Enter Reg. Number..!"
                        showToast(
                            "Please Enter Reg. Number..!",
                            this@PickUpValidateActivity
                        )
                    } /*else if (remarks_edt.getText().toString().isEmpty()) {

                        remarks_edt.setError("Please Enter Remarks..!");
                        GlobalCalls.showToast("Please Enter Remarks..!", PickUpValidateActivity.this);

                    }*/ else {
                        sendInspData()
                    }
                } else {
                    showToast(
                        "Please Inspect All Services..!",
                        this@PickUpValidateActivity
                    )
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show()
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 100)
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        /*From Gallery*/
        /*if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                bike_img.setImageBitmap(bitmap);
                uploadBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        /*From Camera*/if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_one!!.setImageBitmap(photo)
            uploadBitmap(photo, "1")
        }
        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_two!!.setImageBitmap(photo)
            uploadBitmap(photo, "2")
        }
        if (requestCode == 103 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_three!!.setImageBitmap(photo)
            uploadBitmap(photo, "3")
        }
        if (requestCode == 104 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_four!!.setImageBitmap(photo)
            uploadBitmap(photo, "4")
        }
        if (requestCode == 105 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_five!!.setImageBitmap(photo)
            uploadBitmap(photo, "5")
        }
        if (requestCode == 106 && resultCode == Activity.RESULT_OK) {
            val photo = data!!.extras!!["data"] as Bitmap?
            bike_img_six!!.setImageBitmap(photo)
            uploadBitmap(photo, "6")
        }
    }

    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun uploadBitmap(bitmap: Bitmap?, count: String) {
        val volleyMultipartRequest: VolleyMultipartRequest = object :
            VolleyMultipartRequest(
                Method.POST,
                AppUrls.BASE_URL + AppUrls.SEND_BIKE_IMAGE,
                Response.Listener { response ->
                    showToast("Success", this@PickUpValidateActivity)
                    if (count.equals("1", ignoreCase = true)) {
                        bike_img_two!!.visibility = View.VISIBLE
                    } else if (count.equals("2", ignoreCase = true)) {
                        bike_img_three!!.visibility = View.VISIBLE
                    } else if (count.equals("3", ignoreCase = true)) {
                        bike_img_four!!.visibility = View.VISIBLE
                    } else if (count.equals("4", ignoreCase = true)) {
                        bike_img_five!!.visibility = View.VISIBLE
                    } else if (count.equals("5", ignoreCase = true)) {
                        bike_img_six!!.visibility = View.VISIBLE
                    }
                    try {
                        val json = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers)))
                        val jsonObject = JSONObject(json)
                        val fileId = jsonObject.optString("fileId")
                        val mimeType = jsonObject.optString("mimeType")
                        val fileName = jsonObject.optString("fileName")
                        val filePath = jsonObject.optString("filePath")
                        val fileSize = jsonObject.optString("fileSize")
                        val checksum = jsonObject.optString("checksum")
                        val createdTime = jsonObject.optString("createdTime")
                        val modifiedTime = jsonObject.optString("modifiedTime")
                        Log.d(
                            "ImgData",
                            """
                                $fileId
                                $mimeType
                                $fileName
                                $filePath
                                $fileSize
                                $checksum
                                $createdTime
                                $modifiedTime
                                """.trimIndent()
                        )
                        list.add(fileId)
                        Log.d("LIST", list.toString())
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT)
                        .show()
                }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                // params.put("tags", tags);
                return HashMap()
            }

            override val byteData: Map<String, DataPart>
                protected get() {
                    val params: MutableMap<String, DataPart> =
                        HashMap()
                    val imagename = System.currentTimeMillis()
                    params["file"] = DataPart("$imagename.png", getFileDataFromDrawable(bitmap))
                    return params
                }
        }
        Volley.newRequestQueue(this).add(volleyMultipartRequest)
    }

    private fun sendInspData() {
        val url = AppUrls.BASE_URL + AppUrls.SEND_PICKUP
        val jsonObject = JSONObject()
        try {
            jsonObject.put("bookingId", bookingId)
            val jArray = JSONArray()
            for (i in inspectionModels.indices) {
                val jsonObject1 = JSONObject()
                jsonObject1.put("bookingId", inspectionModels[i].bookingId)
                jsonObject1.put("inspectionId", inspectionModels[i].inspectionId)
                jsonObject1.put("state", inspectionModels[i].state)
                jsonObject1.put("inspectionName", inspectionModels[i].inspectionName)
                jArray.put(jsonObject1)
            }
            jsonObject.put("inspections", jArray)
            val jsArray = JSONArray(list)
            jsonObject.put("fileIds", jsArray)
            jsonObject.put("registrationNumber", reg_edt!!.text.toString())
            jsonObject.put("inspectionWords", remarks_edt!!.text.toString())
            Log.d("Array", "" + jArray)
            Log.d("ServicesObject", "" + jsonObject.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObject,
            Response.Listener { response ->
                Log.d("SuccessResp", response.toString())
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    val code = jsonObject.getString("code")
                    val intent =
                        Intent(this@PickUpValidateActivity, BookingDetailActivity::class.java)
                    intent.putExtra("activity", "BookingDetailActivity")
                    intent.putExtra("bookingId", bookingId)
                    startActivity(intent)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error -> error.printStackTrace() }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@PickUpValidateActivity)
        requestQueue.add(request)
    }

    override fun onBackPressed() {
        val intent = Intent(this@PickUpValidateActivity, MainActivity::class.java)
        startActivity(intent)
    }
}