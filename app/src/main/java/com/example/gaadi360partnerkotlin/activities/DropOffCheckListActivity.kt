package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.adapters.DropOffCheckListAdapter
import com.example.gaadi360partnerkotlin.models.DropOffServicesModel
import com.example.gaadi360partnerkotlin.models.ServiceDetailModel
import com.example.gaadi360partnerkotlin.models.ServicesModel
import com.example.gaadi360partnerkotlin.models.ServicesModel.Companion.CHILD_TYPE
import com.example.gaadi360partnerkotlin.models.ServicesModel.Companion.HEADER_TYPE
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.function.Predicate
import java.util.stream.Collectors
import java.util.stream.IntStream

class DropOffCheckListActivity : AppCompatActivity(),
    View.OnClickListener {
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var bookingId: String? = null
    var jsonObject: JSONObject? = null
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    var booking_txt: TextView? = null
    var drop_off_btn: Button? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var drop_recyclerview: RecyclerView? = null
    var dropOffCheckListAdapter: DropOffCheckListAdapter? = null
    var dropOffServicesModels: ArrayList<DropOffServicesModel> =
        ArrayList<DropOffServicesModel>()
    var serviceDetailModels: ArrayList<ServiceDetailModel> =
        ArrayList<ServiceDetailModel>()
    var servicesModels: MutableList<ServicesModel> =
        ArrayList<ServicesModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drop_off_check_list)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        bookingId = intent.getStringExtra("bookingId")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        booking_txt = findViewById(R.id.booking_txt)
        booking_txt?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        drop_off_btn = findViewById(R.id.drop_off_btn)
        drop_off_btn?.setTypeface(bold)
        drop_off_btn?.setOnClickListener(this)
        drop_recyclerview = findViewById(R.id.drop_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        drop_recyclerview?.setLayoutManager(layoutManager)
        dropOffCheckListAdapter = DropOffCheckListAdapter(servicesModels, this@DropOffCheckListActivity)
        detail
    }

    //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
    private val detail: Unit
        private get() {
            val url: String =
                AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL.toString() + bookingId.toString() + "/detail"
            Log.d("URL", url)
            val stringRequest: StringRequest = object : StringRequest(
                Method.GET,
                url,
                Response.Listener { response ->
                    Log.d("RESP", response)
                    try {
                        jsonObject = JSONObject(response)
                        val bookingId = jsonObject!!.optString("bookingId")
                        booking_txt!!.text = "Booking Id : $bookingId"
                        val userId = jsonObject!!.optString("userId")
                        val orderId = jsonObject!!.optString("orderId")
                        val serviceCenterId = jsonObject!!.optString("serviceCenterId")
                        val bookingType = jsonObject!!.optString("bookingType")
                        val brandId = jsonObject!!.optString("brandId")
                        val brandName = jsonObject!!.optString("brandName")
                        val modelId = jsonObject!!.optString("modelId")
                        val modelName = jsonObject!!.optString("modelName")
                        val kilometerRangeId =
                            jsonObject!!.optString("kilometerRangeId")
                        val bookingDate = jsonObject!!.optString("bookingDate")
                        val bookingTime = jsonObject!!.optString("bookingTime")
                        val pickupAddress = jsonObject!!.optString("pickupAddress")
                        val pickupAddressLatitude =
                            jsonObject!!.optString("pickupAddressLatitude")
                        val pickupAddressLongitude =
                            jsonObject!!.optString("pickupAddressLongitude")
                        val pickedUpTime = jsonObject!!.optString("pickedUpTime")
                        val deliveredTime = jsonObject!!.optString("deliveredTime")
                        val addressType = jsonObject!!.optString("addressType")
                        val estimatedCost = jsonObject!!.optString("estimatedCost")
                        val finalPrice = jsonObject!!.optString("finalPrice")
                        val promocodeAmount = jsonObject!!.optString("promocodeAmount")
                        val initialPaidAmount =
                            jsonObject!!.optString("initialPaidAmount")
                        val paymentStatus = jsonObject!!.optString("paymentStatus")
                        val status = jsonObject!!.optString("status")
                        val deleted = jsonObject!!.optString("deleted")
                        val createdTime = jsonObject!!.optString("createdTime")
                        val modifiedTime = jsonObject!!.optString("modifiedTime")
                        val firstName = jsonObject!!.optString("firstName")
                        val lastName = jsonObject!!.optString("lastName")
                        val serviceCenterName =
                            jsonObject!!.optString("serviceCenterName")
                        val contactMobileNumber =
                            jsonObject!!.optString("contactMobileNumber")
                        val mobile = jsonObject!!.optString("mobile")
                        val rescheduleCount = jsonObject!!.optString("rescheduleCount")
                        val paidAmount = jsonObject!!.optString("paidAmount")
                        val addonsAmount = jsonObject!!.optString("addonsAmount")
                        val services = ""
                        var inspections: String? = ""
                        if (jsonObject!!.has("inspections")) {
                            val jsonArray = jsonObject!!.getJSONArray("inspections")
                            if (Build.VERSION.SDK_INT >= VERSION_CODES.N) {
                                inspections =
                                    IntStream.range(0, jsonArray.length())
                                        .mapToObj { i: Int ->
                                            try {
                                                return@mapToObj jsonArray.getJSONObject(i)
                                                    .optString("inspectionName")
                                            } catch (e: JSONException) {
                                                e.printStackTrace()
                                                return@mapToObj ""
                                            }
                                        }.collect(Collectors.joining("\n"))
                            }
                        }
                        if (jsonObject!!.has("services")) {
                            val jsonArray = jsonObject!!.getJSONArray("services")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val dropOffServicesModel =
                                    DropOffServicesModel()
                                dropOffServicesModel.serviceId=(jsonObject1.optString("serviceId"))
                                dropOffServicesModel.cost=(jsonObject1.optString("cost"))
                                dropOffServicesModel.serviceName=(jsonObject1.optString("serviceName"))
                                dropOffServicesModel.createdTime=(jsonObject1.optString("createdTime"))
                                dropOffServicesModel.modifiedTime=(jsonObject1.optString("modifiedTime"))
                                dropOffServicesModel.bookingId=(jsonObject1.optString("bookingId"))
                                dropOffServicesModel.type=(jsonObject1.optString("type"))
                                servicesModels.add(
                                    ServicesModel(
                                        jsonObject1.optString("serviceId"),
                                        jsonObject1.optString("serviceName"),
                                        jsonObject1.optString("createdTime"),
                                        jsonObject1.optString("modifiedTime"),
                                        jsonObject1.optString("bookingId"),
                                        jsonObject1.optString("cost"),
                                        jsonObject1.optString("type"),
                                        "",
                                        "",
                                        "0",
                                        HEADER_TYPE
                                    )
                                )
                                val jsonArray1 =
                                    jsonObject1.getJSONArray("serviceDetails")
                                for (j in 0 until jsonArray1.length()) {
                                    val jsonObject2 = jsonArray1.getJSONObject(j)
                                    val sdm = ServiceDetailModel()
                                    sdm.serviceId=(jsonObject2.optString("serviceId"))
                                    sdm.perform=(jsonObject2.optString("perform"))
                                    sdm.serviceItem=(jsonObject2.optString("serviceItem"))
                                    Log.d(
                                        "ServiceDetails",
                                        jsonObject2.optString("serviceItem")
                                    )
                                    serviceDetailModels.add(sdm)
                                    servicesModels.add(
                                        ServicesModel(
                                            jsonObject1.optString("serviceId"),
                                            jsonObject1.optString("serviceName"),
                                            jsonObject1.optString("createdTime"),
                                            jsonObject1.optString("modifiedTime"),
                                            jsonObject1.optString("bookingId"),
                                            jsonObject1.optString("cost"),
                                            jsonObject1.optString("type"),
                                            jsonObject2.optString("perform"),
                                            jsonObject2.optString("serviceItem"),
                                            "0",
                                            CHILD_TYPE
                                        )
                                    )
                                }
                                dropOffServicesModel.serviceDetailModels=(serviceDetailModels)
                                dropOffServicesModels.add(dropOffServicesModel)
                            }
                            drop_recyclerview!!.adapter = dropOffCheckListAdapter
                            dropOffCheckListAdapter?.notifyDataSetChanged()
                        }
                        if (jsonObject!!.has("bookingAddonServices")) {
                            val jsonArray =
                                jsonObject!!.getJSONArray("bookingAddonServices")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val dropOffServicesModel =
                                    DropOffServicesModel()
                                dropOffServicesModel.serviceId=(jsonObject1.optString("serviceId"))
                                dropOffServicesModel.cost=(jsonObject1.optString("cost"))
                                dropOffServicesModel.serviceName=(jsonObject1.optString("serviceName"))
                                dropOffServicesModel.createdTime=(jsonObject1.optString("createdTime"))
                                dropOffServicesModel.modifiedTime=(jsonObject1.optString("modifiedTime"))
                                dropOffServicesModel.bookingId=(jsonObject1.optString("bookingId"))
                                dropOffServicesModel.type=(jsonObject1.optString("type"))
                                servicesModels.add(
                                    ServicesModel(
                                        jsonObject1.optString("serviceId"),
                                        jsonObject1.optString("serviceName"),
                                        jsonObject1.optString("createdTime"),
                                        jsonObject1.optString("modifiedTime"),
                                        jsonObject1.optString("bookingId"),
                                        jsonObject1.optString("cost"),
                                        jsonObject1.optString("type"),
                                        "",
                                        "",
                                        "0",
                                        HEADER_TYPE
                                    )
                                )
                                val jsonArray1 =
                                    jsonObject1.getJSONArray("serviceDetails")
                                for (j in 0 until jsonArray1.length()) {
                                    val jsonObject2 = jsonArray1.getJSONObject(j)
                                    val sdm = ServiceDetailModel()
                                    sdm.serviceId=(jsonObject2.optString("serviceId"))
                                    sdm.perform=(jsonObject2.optString("perform"))
                                    sdm.serviceItem=(jsonObject2.optString("serviceItem"))
                                    Log.d(
                                        "ServiceDetails",
                                        jsonObject2.optString("serviceItem")
                                    )
                                    serviceDetailModels.add(sdm)
                                    servicesModels.add(
                                        ServicesModel(
                                            jsonObject1.optString("serviceId"),
                                            jsonObject1.optString("serviceName"),
                                            jsonObject1.optString("createdTime"),
                                            jsonObject1.optString("modifiedTime"),
                                            jsonObject1.optString("bookingId"),
                                            jsonObject1.optString("cost"),
                                            jsonObject1.optString("type"),
                                            jsonObject2.optString("perform"),
                                            jsonObject2.optString("serviceItem"),
                                            "0",
                                            CHILD_TYPE
                                        )
                                    )
                                }
                                dropOffServicesModel.serviceDetailModels=(serviceDetailModels)
                                dropOffServicesModels.add(dropOffServicesModel)
                            }
                        }
                        if (jsonObject!!.has("kilometerRange")) {
                            val jsonObject1 = jsonObject!!.getJSONObject("kilometerRange")
                            val startRange = jsonObject1.optString("startRange")
                            val endRange = jsonObject1.optString("endRange")
                        }
                        val resultDate = convertStringDateToAnotherStringDate(
                            bookingDate,
                            "yyyy-MM-dd",
                            "dd-MM-yyyy"
                        )
                        Log.d("ResultDate", resultDate)
                        val df = SimpleDateFormat("HH:mm:ss")
                        var d: Date? = null
                        try {
                            d = df.parse(bookingTime)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        val cal = Calendar.getInstance()
                        cal.time = d
                        cal.add(Calendar.HOUR, 5)
                        cal.add(Calendar.MINUTE, 30)
                        var newTime = df.format(cal.time)
                        try {
                            val dateFormatter =
                                SimpleDateFormat("HH:mm:ss")
                            val date = dateFormatter.parse(newTime)
                            val timeFormatter =
                                SimpleDateFormat("h:mm a")
                            //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                            newTime = timeFormatter.format(date)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError? ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
            val requestQueue = Volley.newRequestQueue(this@DropOffCheckListActivity)
            requestQueue.add(stringRequest)
        }

    fun convertStringDateToAnotherStringDate(
        stringdate: String,
        stringdateformat: String?,
        returndateformat: String?
    ): String {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }

    override fun onClick(v: View) {
        if (v === close) {
            val intent =
                Intent(this@DropOffCheckListActivity, BookingDetailActivity::class.java)
            intent.putExtra("activity", "DropOffCheckListActivity")
            intent.putExtra("bookingId", bookingId)
            startActivity(intent)
        }
        if (v === drop_off_btn) {
            Log.d("CHCHCH", jsonObject.toString())

            //serviceComplete();
            var status = false
            if (Build.VERSION.SDK_INT >= VERSION_CODES.N) {
                status = servicesModels.stream()
                    .anyMatch(Predicate<ServicesModel> { t: ServicesModel ->
                        !t.serviceItem.isEmpty() && t.status.equals("0")
                    })
            }
            if (!status) {
                serviceComplete()
            } else {
                GlobalCalls.showToast("Please Select All Services..!", this@DropOffCheckListActivity)
            }
        }
    }

    fun serviceComplete() {
        val url: String = AppUrls.BASE_URL + AppUrls.SERVICE_COMPLETED
        val jsonObject = JSONObject()
        try {
            jsonObject.put("bookingId", bookingId)
            if (Build.VERSION.SDK_INT >= VERSION_CODES.N) {
                servicesModels = servicesModels.stream()
                    .sorted(Comparator<ServicesModel> { t1: ServicesModel, t2: ServicesModel ->
                        t1.serviceId.compareTo(t2.serviceId)
                    }).collect(Collectors.toList<Any>()) as MutableList<ServicesModel>
            }
            var serviceId = ""
            val jsonArray = JSONArray()
            var jsonArray1: JSONArray? = null
            var jsonObject1: JSONObject? = null
            for (i in servicesModels.indices) {
                if (!serviceId.equals(servicesModels[i].serviceId, ignoreCase = true)) {
                    serviceId = servicesModels[i].serviceId
                    if (jsonObject1 != null && jsonArray1 != null) {
                        jsonObject1.put("serviceDetails", jsonArray1)
                        jsonArray.put(jsonObject1)
                    }
                    jsonObject1 = JSONObject()
                    jsonObject1.put("serviceId", servicesModels[i].serviceId)
                    jsonObject1.put("serviceName", servicesModels[i].serviceName)
                    jsonObject1.put("createdTime", servicesModels[i].createdTime)
                    jsonObject1.put("modifiedTime", servicesModels[i].modifiedTime)
                    jsonObject1.put("bookingId", servicesModels[i].bookingId)
                    jsonObject1.put("cost", servicesModels[i].cost)
                    jsonObject1.put("type", servicesModels[i].type)
                    jsonArray1 = JSONArray()
                } else {
                    val jsonObject2 = JSONObject()
                    jsonObject2.put("serviceId", servicesModels[i].serviceId)
                    jsonObject2.put("perform", servicesModels[i].perform)
                    jsonObject2.put("serviceItem", servicesModels[i].serviceItem)
                    jsonObject2.put("status", servicesModels[i].status)
                    jsonArray1!!.put(jsonObject2)
                }
            }
            if (jsonObject1 != null && jsonArray1 != null) {
                jsonObject1.put("serviceDetails", jsonArray1)
                jsonArray.put(jsonObject1)
            }
            jsonObject.put("services", jsonArray)
            Log.d("ARRAYDATA", "" + jsonArray)
            Log.d("JAYYYY", "" + jsonObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObject,
            Response.Listener { response ->
                Log.d("SuccessResp", response.toString())
                try {
                    val jsonObj = JSONObject(response.toString())
                    val status = jsonObj.getString("status")
                    val message = jsonObj.getString("message")
                    val code = jsonObj.getString("code")
                    val toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                    //GlobalCalls.showToast(message, this@DropOffCheckListActivity)
                    val intent =
                        Intent(this@DropOffCheckListActivity, BookingDetailActivity::class.java)
                    intent.putExtra("activity", "DropOffCheckListActivity")
                    intent.putExtra("bookingId", bookingId)
                    startActivity(intent)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error: VolleyError -> error.printStackTrace() }
        ) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@DropOffCheckListActivity)
        requestQueue.add(request)
    }
}