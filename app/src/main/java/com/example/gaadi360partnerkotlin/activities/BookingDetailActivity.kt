package com.example.gaadi360partnerkotlin.activities

import `in`.aabhasjindal.otptextview.OtpTextView
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.MainActivity
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.BookingDetailActivity
import com.example.gaadi360partnerkotlin.adapters.*
import com.example.gaadi360partnerkotlin.models.BookingServicesModel
import com.example.gaadi360partnerkotlin.models.InspectionModel
import com.example.gaadi360partnerkotlin.models.PickUpModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls.showToast
import com.example.gaadi360partnerkotlin.utils.NetworkChecking.isConnected
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream

class BookingDetailActivity : AppCompatActivity(), View.OnClickListener,
    OnRefreshListener {
    var close: ImageView? = null
    var redirect_img: ImageView? = null
    private var checkInternet = false
    var bookingId: String? = null
    var accessToken: String? = null
    var brandId: String? = null
    var brandName: String? = null
    var modelId: String? = null
    var modelName: String? = null
    var status: String? = null
    var startRange: String? = null
    var endRange: String? = null
    var mobile: String? = null
    var otp: String? = null
    var serviceCenterId: String? = null
    var userId: String? = null
    var serviceIds: String? = null
    var addOnServiceIds: String? = null
    var activity: String? = null
    var pickupAddressLatitude: String? = null
    var pickupAddressLongitude: String? = null
    var userSessionManager: UserSessionManager? = null
    var toolbar_title: TextView? = null
    var booking_txt: TextView? = null
    var brand_txt: TextView? = null
    var model_txt: TextView? = null
    var date_txt: TextView? = null
    var time_txt: TextView? = null
    var service_detail_txt: TextView? = null
    var addon_service_txt: TextView? = null
    var pick_date_txt: TextView? = null
    var pick_time_txt: TextView? = null
    var photos_txt: TextView? = null
    var payment_status_txt: TextView? = null
    var pickup_txt: TextView? = null
    var final_price_txt: TextView? = null
    var bike_txt: TextView? = null
    var service_txt: TextView? = null
    var address_txt: TextView? = null
    var est_price_txt: TextView? = null
    var est_txt: TextView? = null
    var tot_txt: TextView? = null
    var init_txt: TextView? = null
    var init_price_txt: TextView? = null
    var disc_txt: TextView? = null
    var dicount_txt: TextView? = null
    var addon_txt: TextView? = null
    var addon_price_txt: TextView? = null
    var insp_txt: TextView? = null
    var remarks_txt: TextView? = null
    var remark_txt: TextView? = null
    var moving_txt: TextView? = null
    var kilometers_txt: TextView? = null
    var customer_mobile_txt: TextView? = null
    var reg_txt: TextView? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var discount_tr: TableRow? = null
    var addon_tr: TableRow? = null
    var loc_rl: RelativeLayout? = null
    var photo_rl: RelativeLayout? = null
    var addon_rl: RelativeLayout? = null
    var insp_rl: RelativeLayout? = null
    var remarks_rl: RelativeLayout? = null
    var assign_btn: Button? = null
    var addon_btn: Button? = null
    var completed_btn: Button? = null
    var addon_ll: LinearLayout? = null
    var addon_recyclerview: RecyclerView? = null
    var photos_recyclerview: RecyclerView? = null
    var insp_recyclerview: RecyclerView? = null
    var addonAdapter: AddonAdapter? = null
    var addOnModels: MutableList<BookingServicesModel> =
        ArrayList()
    var inspAdapter: InspAdapter? = null
    var inspectionModels: MutableList<InspectionModel> =
        ArrayList()
    var photosAdapter: PhotosAdapter? = null
    var mylist: MutableList<String> = ArrayList()
    var dialog: AlertDialog? = null
    var pickUpAdapter: PickUpAdapter? = null
    var pickUpModels = ArrayList<PickUpModel>()
    var mechanicAdapter: MechanicAdapter? = null
    var mechanicModels = ArrayList<PickUpModel>()
    var otpTextView: OtpTextView? = null
    var jsonObject: JSONObject? = null
    private var swipe: SwipeRefreshLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_detail)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails =
            userSessionManager!!.userDetails
        accessToken = userDetails!![UserSessionManager.KEY_ACCSES]
        activity = intent.getStringExtra("activity")
        bookingId = intent.getStringExtra("bookingId")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        swipe = findViewById(R.id.swipe)
        swipe?.setColorSchemeResources(R.color.orange, R.color.colorPrimary)
        swipe?.setOnRefreshListener(this)
        redirect_img = findViewById(R.id.redirect_img)
        redirect_img?.setOnClickListener(this)
        booking_txt = findViewById(R.id.booking_txt)
        booking_txt?.setTypeface(bold)
        customer_mobile_txt = findViewById(R.id.customer_mobile_txt)
        customer_mobile_txt?.setTypeface(bold)
        customer_mobile_txt?.setPaintFlags(customer_mobile_txt?.getPaintFlags()?.or(Paint.UNDERLINE_TEXT_FLAG)!!)
        customer_mobile_txt?.setOnClickListener(this)
        bike_txt = findViewById(R.id.bike_txt)
        bike_txt?.setTypeface(bold)
        brand_txt = findViewById(R.id.brand_txt)
        brand_txt?.setTypeface(regular)
        model_txt = findViewById(R.id.model_txt)
        model_txt?.setTypeface(regular)
        reg_txt = findViewById(R.id.reg_txt)
        reg_txt?.setTypeface(regular)
        date_txt = findViewById(R.id.date_txt)
        date_txt?.setTypeface(regular)
        time_txt = findViewById(R.id.time_txt)
        time_txt?.setTypeface(regular)
        service_txt = findViewById(R.id.service_txt)
        service_txt?.setTypeface(bold)
        service_detail_txt = findViewById(R.id.service_detail_txt)
        service_detail_txt?.setTypeface(regular)
        remarks_txt = findViewById(R.id.remarks_txt)
        remarks_txt?.setTypeface(bold)
        remark_txt = findViewById(R.id.remark_txt)
        remark_txt?.setTypeface(regular)
        addon_service_txt = findViewById(R.id.addon_service_txt)
        addon_service_txt?.setTypeface(bold)
        insp_recyclerview = findViewById(R.id.insp_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        insp_recyclerview?.setLayoutManager(layoutManager)
        inspAdapter = InspAdapter(inspectionModels, this@BookingDetailActivity, R.layout.row_insp)
        addon_recyclerview = findViewById(R.id.addon_recyclerview)
        val layoutManager1: RecyclerView.LayoutManager =
            LinearLayoutManager(applicationContext)
        addon_recyclerview?.setLayoutManager(layoutManager1)
        addonAdapter = AddonAdapter(addOnModels, this@BookingDetailActivity, R.layout.row_addon)
        insp_rl = findViewById(R.id.insp_rl)
        remarks_rl = findViewById(R.id.remarks_rl)
        addon_rl = findViewById(R.id.addon_rl)
        photo_rl = findViewById(R.id.photo_rl)
        loc_rl = findViewById(R.id.loc_rl)
        loc_rl?.setOnClickListener(this)
        photos_recyclerview = findViewById(R.id.photos_recyclerview)
        photosAdapter = PhotosAdapter(mylist, this@BookingDetailActivity, R.layout.row_photos)
        photos_recyclerview?.setLayoutManager(
            GridLayoutManager(
                this,
                1,
                GridLayoutManager.HORIZONTAL,
                false
            )
        )
        address_txt = findViewById(R.id.address_txt)
        address_txt?.setTypeface(bold)
        pick_date_txt = findViewById(R.id.pick_date_txt)
        pick_date_txt?.setTypeface(regular)
        pick_time_txt = findViewById(R.id.pick_time_txt)
        pick_time_txt?.setTypeface(regular)
        pickup_txt = findViewById(R.id.pickup_txt)
        pickup_txt?.setTypeface(regular)
        pickup_txt?.setOnClickListener(this)
        discount_tr = findViewById(R.id.discount_tr)
        addon_tr = findViewById(R.id.addon_tr)
        est_txt = findViewById(R.id.est_txt)
        est_txt?.setTypeface(bold)
        est_price_txt = findViewById(R.id.est_price_txt)
        est_price_txt?.setTypeface(bold)
        init_txt = findViewById(R.id.init_txt)
        init_txt?.setTypeface(bold)
        init_price_txt = findViewById(R.id.init_price_txt)
        init_price_txt?.setTypeface(bold)
        disc_txt = findViewById(R.id.disc_txt)
        disc_txt?.setTypeface(bold)
        dicount_txt = findViewById(R.id.dicount_txt)
        dicount_txt?.setTypeface(bold)
        insp_txt = findViewById(R.id.insp_txt)
        insp_txt?.setTypeface(bold)
        addon_txt = findViewById(R.id.addon_txt)
        addon_txt?.setTypeface(bold)
        addon_price_txt = findViewById(R.id.addon_price_txt)
        addon_price_txt?.setTypeface(bold)
        tot_txt = findViewById(R.id.tot_txt)
        tot_txt?.setTypeface(bold)
        final_price_txt = findViewById(R.id.final_price_txt)
        final_price_txt?.setTypeface(bold)
        photos_txt = findViewById(R.id.photos_txt)
        photos_txt?.setTypeface(bold)
        payment_status_txt = findViewById(R.id.payment_status_txt)
        payment_status_txt?.setTypeface(bold)
        moving_txt = findViewById(R.id.moving_txt)
        moving_txt?.setTypeface(bold)
        kilometers_txt = findViewById(R.id.kilometers_txt)
        kilometers_txt?.setTypeface(regular)
        assign_btn = findViewById(R.id.assign_btn)
        assign_btn?.setTypeface(bold)
        assign_btn?.setOnClickListener(this)
        addon_ll = findViewById(R.id.addon_ll)
        addon_btn = findViewById(R.id.addon_btn)
        addon_btn?.setTypeface(bold)
        addon_btn?.setOnClickListener(this)
        completed_btn = findViewById(R.id.completed_btn)
        completed_btn?.setTypeface(bold)
        completed_btn?.setOnClickListener(this)
        historyDetail
    }//assign_btn.setText("Delivered");

    //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
    private val historyDetail: Unit
        private get() {
            val url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail"
            Log.d("URL", url)
            val stringRequest: StringRequest = object : StringRequest(
                Method.GET,
                url,
                Response.Listener { response ->
                    Log.d("RESP", response)
                    try {
                        jsonObject = JSONObject(response)
                        val bookingId = jsonObject!!.optString("bookingId")
                        booking_txt!!.text = "Booking Id : $bookingId"
                        userId = jsonObject!!.optString("userId")
                        val orderId = jsonObject!!.optString("orderId")
                        serviceCenterId = jsonObject!!.optString("serviceCenterId")
                        val bookingType = jsonObject!!.optString("bookingType")
                        brandId = jsonObject!!.optString("brandId")
                        brandName = jsonObject!!.optString("brandName")
                        modelId = jsonObject!!.optString("modelId")
                        modelName = jsonObject!!.optString("modelName")
                        val kilometerRangeId =
                            jsonObject!!.optString("kilometerRangeId")
                        val bookingDate = jsonObject!!.optString("bookingDate")
                        val bookingTime = jsonObject!!.optString("bookingTime")
                        val pickupAddress = jsonObject!!.optString("pickupAddress")
                        pickupAddressLatitude = jsonObject!!.optString("pickupAddressLatitude")
                        pickupAddressLongitude = jsonObject!!.optString("pickupAddressLongitude")
                        val pickedUpTime = jsonObject!!.optString("pickedUpTime")
                        val deliveredTime = jsonObject!!.optString("deliveredTime")
                        val addressType = jsonObject!!.optString("addressType")
                        val estimatedCost = jsonObject!!.optString("estimatedCost")
                        val finalPrice = jsonObject!!.optString("finalPrice")
                        val inspectionWords = jsonObject!!.optString("inspectionWords")
                        val promocodeAmount = jsonObject!!.optString("promocodeAmount")
                        val initialPaidAmount =
                            jsonObject!!.optString("initialPaidAmount")
                        val paymentStatus = jsonObject!!.optString("paymentStatus")
                        val movingCondition =
                            jsonObject!!.optBoolean("movingCondition")
                        if (movingCondition) {
                            moving_txt!!.setTextColor(
                                ContextCompat.getColor(
                                    this@BookingDetailActivity,
                                    R.color.green
                                )
                            )
                            moving_txt!!.text = "Vehicle Is In Moving Condition"
                        } else {
                            moving_txt!!.setTextColor(
                                ContextCompat.getColor(
                                    this@BookingDetailActivity,
                                    R.color.red
                                )
                            )
                            moving_txt!!.text = "Vehicle Is Not In Moving Condition"
                        }
                        if (paymentStatus.equals("2", ignoreCase = true)) {
                            payment_status_txt!!.text = "Final Payment Done"
                        } else {
                            payment_status_txt!!.setTextColor(Color.RED)
                            payment_status_txt!!.text = "Final Payment Pending"
                        }
                        status = jsonObject!!.optString("status")
                        val deleted = jsonObject!!.optString("deleted")
                        val createdTime = jsonObject!!.optString("createdTime")
                        val modifiedTime = jsonObject!!.optString("modifiedTime")
                        val firstName = jsonObject!!.optString("firstName")
                        val lastName = jsonObject!!.optString("lastName")
                        val serviceCenterName =
                            jsonObject!!.optString("serviceCenterName")
                        val contactMobileNumber =
                            jsonObject!!.optString("contactMobileNumber")
                        mobile = jsonObject!!.optString("mobile")
                        val rescheduleCount = jsonObject!!.optString("rescheduleCount")
                        val paidAmount = jsonObject!!.optString("paidAmount")
                        val addonsAmount = jsonObject!!.optString("addonsAmount")
                        val registrationNumber =
                            jsonObject!!.optString("registrationNumber")
                        reg_txt!!.text = registrationNumber
                        customer_mobile_txt!!.text = mobile
                        if (inspectionWords.isEmpty()) {
                            remarks_rl!!.visibility = View.GONE
                        } else {
                            remarks_rl!!.visibility = View.VISIBLE
                            remark_txt!!.text = inspectionWords
                        }
                        if (jsonObject!!.has("fileIds")) {
                            photos_txt!!.visibility = View.VISIBLE
                            photo_rl!!.visibility = View.VISIBLE
                            photos_recyclerview!!.visibility = View.VISIBLE
                            val jsonArray = jsonObject!!.getJSONArray("fileIds")
                            for (i in 0 until jsonArray.length()) {
                                val data = jsonArray[i].toString()
                                mylist.add(AppUrls.IMAGE_URL + data)
                            }
                            Log.d("PHOTOS", mylist.toString())
                            photos_recyclerview!!.adapter = photosAdapter
                            photosAdapter!!.notifyDataSetChanged()
                        } else {
                            photos_txt!!.visibility = View.GONE
                            photo_rl!!.visibility = View.GONE
                            photos_recyclerview!!.visibility = View.GONE
                        }
                        var services: String? = ""
                        var inspections: String? = ""
                        if (jsonObject!!.has("services")) {
                            val jsonArray = jsonObject!!.getJSONArray("services")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                services = IntStream.range(0, jsonArray.length())
                                    .mapToObj { i: Int ->
                                        try {
                                            return@mapToObj jsonArray.getJSONObject(i)
                                                .optString("serviceName")
                                        } catch (e: JSONException) {
                                            e.printStackTrace()
                                            return@mapToObj ""
                                        }
                                    }.collect(Collectors.joining("\n"))
                            }
                            service_detail_txt!!.text = services
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                serviceIds = IntStream.range(0, jsonArray.length())
                                    .mapToObj { i: Int ->
                                        try {
                                            return@mapToObj jsonArray.getJSONObject(i)
                                                .optString("serviceId")
                                        } catch (e: JSONException) {
                                            e.printStackTrace()
                                            return@mapToObj ""
                                        }
                                    }.collect(Collectors.joining(","))
                            }
                        }
                        if (jsonObject!!.has("inspections")) {
                            val jsonArray = jsonObject!!.getJSONArray("inspections")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                inspections =
                                    IntStream.range(0, jsonArray.length())
                                        .mapToObj { i: Int ->
                                            try {
                                                return@mapToObj jsonArray.getJSONObject(i)
                                                    .optString("inspectionName")
                                            } catch (e: JSONException) {
                                                e.printStackTrace()
                                                return@mapToObj ""
                                            }
                                        }.collect(Collectors.joining("\n"))
                            }
                            if (jsonArray.length() > 0) {
                                insp_rl!!.visibility = View.VISIBLE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val inspectionModel = InspectionModel()
                                    inspectionModel.inspectionId =
                                        jsonObject1.getString("inspectionId")
                                    inspectionModel.bookingId = jsonObject1.getString("bookingId")
                                    inspectionModel.state = jsonObject1.getString("state")
                                    inspectionModel.inspectionName =
                                        jsonObject1.getString("inspectionName")
                                    inspectionModels.add(inspectionModel)
                                }
                                insp_recyclerview!!.adapter = inspAdapter
                                inspAdapter!!.notifyDataSetChanged()
                            } else {
                                insp_rl!!.visibility = View.GONE
                            }
                        }
                        if (jsonObject!!.has("bookingAddonServices")) {
                            val jsonArray =
                                jsonObject!!.getJSONArray("bookingAddonServices")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                addOnServiceIds =
                                    IntStream.range(0, jsonArray.length())
                                        .mapToObj { i: Int ->
                                            try {
                                                if (!jsonArray.getJSONObject(i)
                                                        .optString("status")
                                                        .equals("2", ignoreCase = true)
                                                ) return@mapToObj jsonArray.getJSONObject(i)
                                                    .optString("serviceId")
                                            } catch (e: JSONException) {
                                                e.printStackTrace()
                                                return@mapToObj ""
                                            }
                                            ""
                                        }.collect(Collectors.joining(","))
                            }
                            if (jsonArray.length() > 0) {
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject1 = jsonArray.getJSONObject(i)
                                    val servicesModel = BookingServicesModel()
                                    servicesModel.serviceId = jsonObject1.getString("serviceId")
                                    servicesModel.cost = jsonObject1.getString("cost")
                                    servicesModel.serviceName = jsonObject1.getString("serviceName")
                                    servicesModel.status = jsonObject1.getString("status")
                                    addOnModels.add(servicesModel)
                                }
                                addon_recyclerview!!.adapter = addonAdapter
                                addonAdapter!!.notifyDataSetChanged()
                            } else {
                                addon_rl!!.visibility = View.GONE
                            }
                        }
                        if (jsonObject!!.has("kilometerRange")) {
                            val jsonObject1 = jsonObject!!.getJSONObject("kilometerRange")
                            startRange = jsonObject1.optString("startRange")
                            endRange = jsonObject1.optString("endRange")
                            kilometers_txt!!.text = "$startRange - $endRange km"
                        }
                        if (pickedUpTime.isEmpty()) {
                            pick_date_txt!!.visibility = View.GONE
                        } else {
                            pick_date_txt!!.visibility = View.VISIBLE
                            pick_date_txt!!.text = "Pick Up : $pickedUpTime"
                        }
                        if (deliveredTime.isEmpty()) {
                            pick_time_txt!!.visibility = View.GONE
                        } else {
                            pick_time_txt!!.visibility = View.VISIBLE
                            pick_time_txt!!.text = "Delivery : $deliveredTime"
                        }
                        val resultDate = convertStringDateToAnotherStringDate(
                            bookingDate,
                            "yyyy-MM-dd",
                            "dd-MM-yyyy"
                        )
                        Log.d("ResultDate", resultDate)
                        val df = SimpleDateFormat("HH:mm:ss")
                        var d: Date? = null
                        try {
                            d = df.parse(bookingTime)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        val cal = Calendar.getInstance()
                        cal.time = d
                        cal.add(Calendar.HOUR, 5)
                        cal.add(Calendar.MINUTE, 30)
                        var newTime = df.format(cal.time)
                        try {
                            val dateFormatter =
                                SimpleDateFormat("HH:mm:ss")
                            val date = dateFormatter.parse(newTime)
                            val timeFormatter =
                                SimpleDateFormat("h:mm a")
                            //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                            newTime = timeFormatter.format(date)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        brand_txt!!.text = brandName
                        model_txt!!.text = modelName
                        date_txt!!.text = resultDate
                        time_txt!!.text = newTime
                        pickup_txt!!.text = pickupAddress
                        if (addonsAmount.equals("0", ignoreCase = true)) {
                            addon_tr!!.visibility = View.GONE
                        } else {
                            addon_tr!!.visibility = View.VISIBLE
                            addon_price_txt!!.text = "\u20B9 $addonsAmount"
                        }
                        if (promocodeAmount.isEmpty() || promocodeAmount.equals(
                                "0",
                                ignoreCase = true
                            )
                        ) {
                            discount_tr!!.visibility = View.GONE
                        } else {
                            discount_tr!!.visibility = View.VISIBLE
                            dicount_txt!!.text = "\u20B9 $promocodeAmount"
                        }
                        est_price_txt!!.text = "\u20B9 $estimatedCost"
                        init_price_txt!!.text = "\u20B9 $initialPaidAmount"
                        final_price_txt!!.text = "\u20B9 $paidAmount"
                        if (status.equals("1", ignoreCase = true)) {
                            assign_btn!!.text = "Assign Pickup"
                        } else if (status.equals("2", ignoreCase = true)) {
                            assign_btn!!.text = "Pickup"
                        } else if (status.equals("3", ignoreCase = true)) {
                            assign_btn!!.text = "Assign Mechanic"
                        } else if (status.equals("4", ignoreCase = true)) {
                            assign_btn!!.visibility = View.GONE
                            assign_btn!!.text = "Service Completed"
                            addon_ll!!.visibility = View.VISIBLE
                        } else if (status.equals("5", ignoreCase = true)) {
                            assign_btn!!.text = "Drop Off"
                        } else if (status.equals("6", ignoreCase = true)) {
                            assign_btn!!.text = "Drop Off"
                            payment_status_txt!!.visibility = View.VISIBLE
                        } else if (status.equals("7", ignoreCase = true)) {
                            assign_btn!!.visibility = View.GONE
                            payment_status_txt!!.visibility = View.VISIBLE
                            //assign_btn.setText("Delivered");
                        } else if (status.equals("8", ignoreCase = true)) {
                            assign_btn!!.text = "Deliver"
                            payment_status_txt!!.visibility = View.VISIBLE
                        } else if (status.equals("9", ignoreCase = true)) {
                            assign_btn!!.visibility = View.GONE
                            addon_ll!!.visibility = View.VISIBLE
                            completed_btn!!.visibility = View.GONE
                        } else if (status.equals("10", ignoreCase = true)) {
                            assign_btn!!.text = "Start To PickUp"
                        } else if (status.equals("11", ignoreCase = true)) {
                            assign_btn!!.text = "Booking Reached SST"
                        } else if (status.equals("12", ignoreCase = true)) {
                            assign_btn!!.text = "Cancelled"
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError? ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
            val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
            requestQueue.add(stringRequest)
        }

    fun convertStringDateToAnotherStringDate(
        stringdate: String,
        stringdateformat: String?,
        returndateformat: String?
    ): String {
        return try {
            val date =
                SimpleDateFormat(stringdateformat).parse(stringdate)
            SimpleDateFormat(returndateformat).format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            stringdate
        }
    }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                if (activity.equals("NewHistoryAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, MainActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("HistoryAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, HistoryActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("DeliveredHistoryAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, DeliveredActivity::class.java)
                    startActivity(intent)
                } else if (activity.equals("NotificationAdapter", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, NotificationActivity::class.java)
                    startActivity(intent)
                } else {
                    val intent =
                        Intent(this@BookingDetailActivity, MainActivity::class.java)
                    startActivity(intent)
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === customer_mobile_txt) {
            if (checkInternet) {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse("tel:" + Uri.encode(mobile))
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(callIntent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === loc_rl || v === pickup_txt || v === redirect_img) {
            if (checkInternet) {
                val uri =
                    "http://maps.google.com/maps?q=loc:$pickupAddressLatitude,$pickupAddressLongitude (Customer)"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === assign_btn) {
            if (checkInternet) {
                if (status.equals("1", ignoreCase = true)) {
                    pickUpData
                }
                if (status.equals("2", ignoreCase = true)) {
                    getOtp()
                }
                if (status.equals("3", ignoreCase = true)) {
                    mechanicData
                }
                if (status.equals("4", ignoreCase = true)) {
                    val intent =
                        Intent(this@BookingDetailActivity, DropOffCheckListActivity::class.java)
                    intent.putExtra("bookingId", bookingId)
                    startActivity(intent)

                    //serviceComplete();
                }
                if (status.equals("5", ignoreCase = true)) {
                    dropOffStart()
                }
                if (status.equals("8", ignoreCase = true)) {
                    deliveryOtp
                }
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === addon_btn) {
            val intent =
                Intent(this@BookingDetailActivity, AddonServicesActivity::class.java)
            intent.putExtra("serviceCenterId", serviceCenterId)
            intent.putExtra("bookingId", bookingId)
            intent.putExtra("userId", userId)
            intent.putExtra("serviceIds", "$serviceIds,$addOnServiceIds")
            intent.putExtra("jsonObj", jsonObject.toString())
            startActivity(intent)
        }
        if (v === completed_btn) {
            val intent =
                Intent(this@BookingDetailActivity, DropOffCheckListActivity::class.java)
            intent.putExtra("bookingId", bookingId)
            startActivity(intent)

            //serviceComplete();
        }
    }

    private val pickUpData: Unit
        private get() {
            pickUpModels.clear()
            val url = AppUrls.BASE_URL + AppUrls.ASSIGN_PICKUP
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET, url,
                    Response.Listener { response: String? ->
                        try {
                            val jsonArray = JSONArray(response)
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject = jsonArray.getJSONObject(i)
                                val pickUpModel = PickUpModel()
                                pickUpModel.userId = jsonObject.optString("userId")
                                pickUpModel.firstName = jsonObject.optString("firstName")
                                pickUpModel.username = jsonObject.optString("username")
                                pickUpModel.locked = jsonObject.optString("locked")
                                pickUpModel.enabled = jsonObject.optString("enabled")
                                pickUpModel.expired = jsonObject.optString("expired")
                                pickUpModel.authority = jsonObject.optString("authority")
                                pickUpModel.email = jsonObject.optString("email")
                                pickUpModel.mobile = jsonObject.optString("mobile")
                                pickUpModel.emailVerified = jsonObject.optString("emailVerified")
                                pickUpModel.mobileVerified = jsonObject.optString("mobileVerified")
                                pickUpModel.createdTime = jsonObject.optString("createdTime")
                                pickUpModel.modifiedTime = jsonObject.optString("modifiedTime")
                                pickUpModels.add(pickUpModel)
                            }
                            pickUp()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error: VolleyError? -> }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
            requestQueue.add(stringRequest)
        }

    private fun pickUp() {
        val builder =
            AlertDialog.Builder(this@BookingDetailActivity)
        val inflater = this@BookingDetailActivity.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.pickup_dialog, null)
        val pick_title = dialog_layout.findViewById<TextView>(R.id.pick_title)
        pick_title.setTypeface(regular)
        pick_title.text = "Pick Boys"
        val pick_recyclerview: RecyclerView = dialog_layout.findViewById(R.id.pick_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        pick_recyclerview.layoutManager = layoutManager
        pickUpAdapter = PickUpAdapter(pickUpModels, this@BookingDetailActivity, R.layout.row_pickup)
        pick_recyclerview.adapter = pickUpAdapter
        builder.setView(dialog_layout).setNegativeButton(
            "CANCEL"
        ) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        dialog = builder.create()
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun assignPickUp(id: String) {
        val url =
            AppUrls.BASE_URL + "secure/booking/" + bookingId + AppUrls.ASSIGNED_PICKUP + id
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET,
            url,
            object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.optString("status")
                        val code = jsonObject.optString("code")
                        val message = jsonObject.optString("message")
                        if (status.equals("2000", ignoreCase = true)) {
                            dialog!!.dismiss()
                            assign_btn!!.text = "Start PickUp"
                            val intent = Intent(
                                this@BookingDetailActivity,
                                BookingDetailActivity::class.java
                            )
                            intent.putExtra("activity", "BookingDetailActivity")
                            intent.putExtra("bookingId", bookingId)
                            startActivity(intent)
                        } else {
                            showToast(message, this@BookingDetailActivity)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            },
            Response.ErrorListener { error: VolleyError? ->
                if (error is TimeoutError || error is NoConnectionError) {
                } else if (error is AuthFailureError) {
                } else if (error is ServerError) {
                } else if (error is NetworkError) {
                } else if (error is ParseError) {
                }
            }
        ) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
        requestQueue.add(stringRequest)
    }

    private fun getOtp() {
        val url =
            AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile
        val stringRequest: StringRequest =
            object : StringRequest(
                Method.GET, url,
                Response.Listener { response: String? ->
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.optString("status")
                        val code = jsonObject.optString("code")
                        val message = jsonObject.optString("message")
                        if (status.equals("2000", ignoreCase = true)) {
                            otpDialog()
                        } else {
                            showToast(message, this@BookingDetailActivity)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError ->
                    showToast(
                        error.message,
                        this@BookingDetailActivity
                    )
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
        val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
        requestQueue.add(stringRequest)
    }

    private fun otpDialog() {
        val builder =
            AlertDialog.Builder(this@BookingDetailActivity)
        builder.setCancelable(false)
        val inflater = this@BookingDetailActivity.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.otp_dialog, null)
        val otp_txt = dialog_layout.findViewById<TextView>(R.id.otp_txt)
        otp_txt.setTypeface(bold)
        otpTextView = dialog_layout.findViewById(R.id.otpTextView)
        val resend_txt = dialog_layout.findViewById<TextView>(R.id.resend_txt)
        resend_txt.setTypeface(regular)
        resend_txt.paintFlags = resend_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        /*smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();*/resend_txt.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(view: View) {
                val url =
                    AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile
                val stringRequest: StringRequest =
                    object : StringRequest(
                        Method.GET, url,
                        Response.Listener { response: String? ->
                            try {
                                val jsonObject = JSONObject(response)
                                val status = jsonObject.optString("status")
                                val code = jsonObject.optString("code")
                                val message = jsonObject.optString("message")
                                otpDialog()
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { error: VolleyError? -> }
                    ) {
                        override fun getHeaders(): Map<String, String> {
                            val headers: MutableMap<String, String> =
                                HashMap()
                            headers["Authorization"] = "Bearer $accessToken"
                            return headers
                        }
                    }
                val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
                requestQueue.add(stringRequest)
            }
        })
        val verify_btn =
            dialog_layout.findViewById<Button>(R.id.verify_btn)
        verify_btn.setTypeface(bold)
        verify_btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                otp = otpTextView?.getOTP()
                val url =
                    AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + otp + "&bookingId=" + bookingId
                Log.d("VERIFY", url)
                val otp: String = otpTextView?.getOTP()!!
                val stringRequest: StringRequest =
                    object : StringRequest(
                        Method.GET, url,
                        object : Response.Listener<String?> {
                            override fun onResponse(response: String?) {
                                try {
                                    val jsonObject = JSONObject(response)
                                    val status = jsonObject.optString("status")
                                    val code = jsonObject.optString("code")
                                    val message = jsonObject.optString("message")
                                    val intent = Intent(
                                        this@BookingDetailActivity,
                                        PickUpValidateActivity::class.java
                                    )
                                    intent.putExtra("bookingId", bookingId)
                                    startActivity(intent)
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }
                        },
                        object : Response.ErrorListener {
                            override fun onErrorResponse(error: VolleyError) {
                                error.message
                                otpTextView?.showError()
                            }
                        }) {
                        override fun getHeaders(): Map<String, String> {
                            val headers: MutableMap<String, String> =
                                HashMap()
                            headers["Authorization"] = "Bearer $accessToken"
                            return headers
                        }
                    }
                val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
                requestQueue.add(stringRequest)
            }
        })
        builder.setView(dialog_layout)
            .setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                override fun onClick(dialogInterface: DialogInterface, i: Int) {
                    dialogInterface.dismiss()
                }
            })
        dialog = builder.create()
        dialog!!.show()
    }

    private val mechanicData: Unit
        private get() {
            mechanicModels.clear()
            val url = AppUrls.BASE_URL + AppUrls.SEND_MECHANIC
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET, url,
                    Response.Listener { response: String? ->
                        try {
                            val jsonArray = JSONArray(response)
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject = jsonArray.getJSONObject(i)
                                val pickUpModel = PickUpModel()
                                pickUpModel.userId = jsonObject.optString("userId")
                                pickUpModel.firstName = jsonObject.optString("firstName")
                                pickUpModel.username = jsonObject.optString("username")
                                pickUpModel.locked = jsonObject.optString("locked")
                                pickUpModel.enabled = jsonObject.optString("enabled")
                                pickUpModel.expired = jsonObject.optString("expired")
                                pickUpModel.authority = jsonObject.optString("authority")
                                pickUpModel.email = jsonObject.optString("email")
                                pickUpModel.mobile = jsonObject.optString("mobile")
                                pickUpModel.emailVerified = jsonObject.optString("emailVerified")
                                pickUpModel.mobileVerified = jsonObject.optString("mobileVerified")
                                pickUpModel.createdTime = jsonObject.optString("createdTime")
                                pickUpModel.modifiedTime = jsonObject.optString("modifiedTime")
                                mechanicModels.add(pickUpModel)
                            }
                            mechanicAssign()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error: VolleyError? -> }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
            requestQueue.add(stringRequest)
        }

    private fun mechanicAssign() {
        val builder =
            AlertDialog.Builder(this@BookingDetailActivity)
        val inflater = this@BookingDetailActivity.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.pickup_dialog, null)
        val pick_title = dialog_layout.findViewById<TextView>(R.id.pick_title)
        pick_title.setTypeface(regular)
        pick_title.text = "Assign Mechanic"
        val pick_recyclerview: RecyclerView = dialog_layout.findViewById(R.id.pick_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        pick_recyclerview.layoutManager = layoutManager
        mechanicAdapter =
            MechanicAdapter(mechanicModels, this@BookingDetailActivity, R.layout.row_pickup)
        pick_recyclerview.adapter = mechanicAdapter
        builder.setView(dialog_layout).setNegativeButton(
            "CANCEL"
        ) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        dialog = builder.create()
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun assignMechanic(id: String) {
        val url =
            AppUrls.BASE_URL + "secure/booking/" + bookingId + AppUrls.ASSIGN_MECHANIC + id
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET,
            url,
            object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.optString("status")
                        val code = jsonObject.optString("code")
                        val message = jsonObject.optString("message")
                        if (status.equals("2000", ignoreCase = true)) {
                            dialog!!.dismiss()
                            assign_btn!!.visibility = View.GONE
                            val intent = Intent(
                                this@BookingDetailActivity,
                                BookingDetailActivity::class.java
                            )
                            intent.putExtra("activity", "BookingDetailActivity")
                            intent.putExtra("bookingId", bookingId)
                            startActivity(intent)
                        } else {
                            showToast(message, this@BookingDetailActivity)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            },
            Response.ErrorListener { error: VolleyError? ->
                if (error is TimeoutError || error is NoConnectionError) {
                } else if (error is AuthFailureError) {
                } else if (error is ServerError) {
                } else if (error is NetworkError) {
                } else if (error is ParseError) {
                }
            }
        ) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
        requestQueue.add(stringRequest)
    }

    fun serviceComplete() {
        val url = AppUrls.BASE_URL + AppUrls.SERVICE_COMPLETED
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObject,
            object : Response.Listener<JSONObject> {
                override fun onResponse(response: JSONObject) {
                    Log.d("SuccessResp", response.toString())
                    try {
                        val jsonObj = JSONObject(response.toString())
                        val status = jsonObj.getString("status")
                        val message = jsonObj.getString("message")
                        val code = jsonObj.getString("code")
                        showToast(message, this@BookingDetailActivity)
                        val intent =
                            Intent(this@BookingDetailActivity, DropOffCheckListActivity::class.java)
                        intent.putExtra("bookingId", bookingId)
                        startActivity(intent)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            },
            Response.ErrorListener { error: VolleyError -> error.printStackTrace() }
        ) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
        requestQueue.add(request)
    }

    private fun dropOffStart() {
        val url = AppUrls.BASE_URL + AppUrls.DROP_OFF + bookingId + "/drop-off"
        val stringRequest: StringRequest =
            object : StringRequest(
                Method.GET,
                url,
                Response.Listener { response ->
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.optString("status")
                        val code = jsonObject.optString("code")
                        val message = jsonObject.optString("message")
                        val toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                        //showToast(message, this@BookingDetailActivity)
                        val intent = Intent(
                            this@BookingDetailActivity,
                            BookingDetailActivity::class.java
                        )
                        intent.putExtra("activity", "BookingDetailActivity")
                        intent.putExtra("bookingId", bookingId)
                        startActivity(intent)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
        val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
        requestQueue.add(stringRequest)
    }

    private val deliveryOtp: Unit
        private get() {
            val url =
                AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET, url,
                    Response.Listener { response: String? ->
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.optString("status")
                            val code = jsonObject.optString("code")
                            val message = jsonObject.optString("message")
                            if (status.equals("2000", ignoreCase = true)) {
                                deliveryOtpDialog()
                            } else {
                                showToast(message, this@BookingDetailActivity)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error: VolleyError? -> }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
            requestQueue.add(stringRequest)
        }

    private fun deliveryOtpDialog() {
        val builder =
            AlertDialog.Builder(this@BookingDetailActivity)
        builder.setCancelable(false)
        val inflater = this@BookingDetailActivity.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.delivery_otp_dialog, null)
        val otp_txt = dialog_layout.findViewById<TextView>(R.id.otp_txt)
        otp_txt.setTypeface(bold)
        otpTextView = dialog_layout.findViewById(R.id.otpTextView)
        val resend_txt = dialog_layout.findViewById<TextView>(R.id.resend_txt)
        resend_txt.setTypeface(regular)
        resend_txt.paintFlags = resend_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        /*smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();*/resend_txt.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(view: View) {
                val url =
                    AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile
                val stringRequest: StringRequest =
                    object : StringRequest(
                        Method.GET, url,
                        Response.Listener { response: String? ->
                            try {
                                val jsonObject = JSONObject(response)
                                val status = jsonObject.optString("status")
                                val code = jsonObject.optString("code")
                                val message = jsonObject.optString("message")
                                otpDialog()
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { error: VolleyError? -> }
                    ) {
                        override fun getHeaders(): Map<String, String> {
                            val headers: MutableMap<String, String> =
                                HashMap()
                            headers["Authorization"] = "Bearer $accessToken"
                            return headers
                        }
                    }
                val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
                requestQueue.add(stringRequest)
            }
        })
        val verify_btn =
            dialog_layout.findViewById<Button>(R.id.verify_btn)
        verify_btn.setTypeface(bold)
        verify_btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                otp = otpTextView?.getOTP()
                //String url = AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + mobile + "&otp=" + otp;
                val url =
                    AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + otp + "&bookingId=" + bookingId
                Log.d("VERIFY", url)
                val otp: String = otpTextView?.getOTP()!!
                val stringRequest: StringRequest =
                    object : StringRequest(
                        Method.GET, url,
                        object : Response.Listener<String?> {
                            override fun onResponse(response: String?) {
                                try {
                                    val jsonObject = JSONObject(response)
                                    val status = jsonObject.optString("status")
                                    val code = jsonObject.optString("code")
                                    val message = jsonObject.optString("message")
                                    val intent = Intent(
                                        this@BookingDetailActivity,
                                        DropOffInspectionsActivity::class.java
                                    )
                                    intent.putExtra("bookingId", bookingId)
                                    startActivity(intent)
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }
                        },
                        object : Response.ErrorListener {
                            override fun onErrorResponse(error: VolleyError) {
                                error.message
                                otpTextView?.showError()
                            }
                        }) {
                        override fun getHeaders(): Map<String, String> {
                            val headers: MutableMap<String, String> =
                                HashMap()
                            headers["Authorization"] = "Bearer $accessToken"
                            return headers
                        }
                    }
                val requestQueue = Volley.newRequestQueue(this@BookingDetailActivity)
                requestQueue.add(stringRequest)
            }
        })
        builder.setView(dialog_layout)
            .setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                override fun onClick(dialogInterface: DialogInterface, i: Int) {
                    dialogInterface.dismiss()
                }
            })
        dialog = builder.create()
        dialog!!.show()
    }

    override fun onBackPressed() {
        if (activity.equals("NewHistoryAdapter", ignoreCase = true)) {
            val intent = Intent(this@BookingDetailActivity, MainActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("HistoryAdapter", ignoreCase = true)) {
            val intent = Intent(this@BookingDetailActivity, HistoryActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("DeliveredHistoryAdapter", ignoreCase = true)) {
            val intent = Intent(this@BookingDetailActivity, DeliveredActivity::class.java)
            startActivity(intent)
        } else if (activity.equals("NotificationAdapter", ignoreCase = true)) {
            val intent =
                Intent(this@BookingDetailActivity, NotificationActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this@BookingDetailActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onRefresh() {
        swipe!!.isRefreshing = false
        val intent = Intent(this@BookingDetailActivity, BookingDetailActivity::class.java)
        intent.putExtra("activity", "BookingDetailActivity")
        intent.putExtra("bookingId", bookingId)
        startActivity(intent)
    }
}