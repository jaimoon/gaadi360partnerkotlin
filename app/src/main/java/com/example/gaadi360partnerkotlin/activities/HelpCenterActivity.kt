package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.HelpCenterActivity
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import com.taishi.flipprogressdialog.FlipProgressDialog
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class HelpCenterActivity : AppCompatActivity(), View.OnClickListener {
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    var name_txt: TextView? = null
    var email_txt: TextView? = null
    var mobile_txt: TextView? = null
    var company_name_txt: TextView? = null
    var address_txt: TextView? = null
    private var checkInternet = false
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var name: String? = null
    var email: String? = null
    var mobile: String? = null
    var companyName: String? = null
    var address: String? = null

    /*ProgressDialog*/
    var imageList: List<Int> = ArrayList()
    var flipProgressDialog: FlipProgressDialog? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_center)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        name_txt = findViewById(R.id.name_txt)
        name_txt?.setTypeface(regular)
        email_txt = findViewById(R.id.email_txt)
        email_txt?.setTypeface(regular)
        mobile_txt = findViewById(R.id.mobile_txt)
        mobile_txt?.setTypeface(regular)
        mobile_txt?.setPaintFlags(mobile_txt?.getPaintFlags()?.or(Paint.UNDERLINE_TEXT_FLAG)!!)
        mobile_txt?.setOnClickListener(this)
        company_name_txt = findViewById(R.id.company_name_txt)
        company_name_txt?.setTypeface(regular)
        address_txt = findViewById(R.id.address_txt)
        address_txt?.setTypeface(regular)
        helpCenterDetails
    }

    private val helpCenterDetails: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.HELP_CENTER
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        try {
                            val jsonObject = JSONObject(response)
                            val helpCenterId = jsonObject.getString("helpCenterId")
                            name = jsonObject.getString("name")
                            email = jsonObject.getString("email")
                            mobile = jsonObject.getString("mobile")
                            companyName = jsonObject.getString("companyName")
                            address = jsonObject.getString("address")
                            Log.d(
                                "HELPCENTER",
                                """
                                $name
                                $email
                                $mobile
                                $companyName
                                $address
                                """.trimIndent()
                            )
                            name_txt!!.text = name
                            email_txt!!.text = email
                            company_name_txt!!.text = companyName
                            mobile_txt!!.text = mobile
                            address_txt!!.text = address
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@HelpCenterActivity)
            requestQueue.add(stringRequest)
        }

    @RequiresApi(api = VERSION_CODES.M)
    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@HelpCenterActivity, SettingActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === mobile_txt) {
            if (checkInternet) {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse("tel:" + Uri.encode(mobile))
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(callIntent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    override fun onBackPressed() {
        if (checkInternet) {
            val intent = Intent(this@HelpCenterActivity, SettingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        } else {
            val snackbar = Snackbar.make(
                window.decorView,
                "Check Internet Connection",
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }
}