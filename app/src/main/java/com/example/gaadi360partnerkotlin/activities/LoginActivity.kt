package com.example.gaadi360partnerkotlin.activities

import `in`.aabhasjindal.otptextview.OtpTextView
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.MainActivity
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.LoginActivity
import com.example.gaadi360partnerkotlin.adapters.SliderAdapter
import com.example.gaadi360partnerkotlin.models.ServiceTypeImageModel
import com.example.gaadi360partnerkotlin.models.SliderModel
import com.example.gaadi360partnerkotlin.utils.*
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import com.stfalcon.smsverifycatcher.OnSmsCatchListener
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher
import com.taishi.flipprogressdialog.FlipProgressDialog
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var mobile_til: TextInputLayout? = null
    var mobile_edt: EditText? = null
    var login_btn: Button? = null
    var otpTextView: OtpTextView? = null
    var smsVerifyCatcher: SmsVerifyCatcher? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var mobile: String? = null
    var otp: String? = null
    var firstName: String? = null
    var username: String? = null
    var authority: String? = null
    var email: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null
    var accessToken: String? = null
    var mediaSecret: String? = null
    var fcmToken: String? = null
    var imei: String? = null
    var user_image: String? = null
    var locked: Boolean? = null
    var enabled: Boolean? = null
    var expired: Boolean? = null
    var emailVerified: Boolean? = null
    var mobileVerified: Boolean? = null
    var userId: Int? = null
    var userSessionManager: UserSessionManager? = null
    var dialog: AlertDialog? = null
    private var exit = false
    var sliderView: SliderView? = null
    private var adapter: SliderAdapter? = null

    /*ProgressDialog*/
    var imageList: MutableList<Int> = ArrayList()
    var flipProgressDialog: FlipProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        imei = Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
        Log.d("IMEI", imei)
        sliderView = findViewById(R.id.imageSlider)
        adapter = SliderAdapter(this)
        sliderView?.setSliderAdapter(adapter!!)
        sliderView?.setIndicatorAnimation(IndicatorAnimations.SLIDE) //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView?.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView?.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT)
        sliderView?.setIndicatorSelectedColor(Color.WHITE)
        sliderView?.setIndicatorUnselectedColor(Color.GRAY)
        sliderView?.setScrollTimeInSec(3)
        sliderView?.setAutoCycle(true)
        sliderView?.startAutoCycle()
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog!!.setImageList(imageList)
        flipProgressDialog!!.setCanceledOnTouchOutside(true)
        flipProgressDialog!!.setDimAmount(0.8f) //0.0f
        flipProgressDialog!!.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog!!.setBackgroundAlpha(0.2f)
        flipProgressDialog!!.setBorderStroke(0)
        flipProgressDialog!!.setBorderColor(-1)
        flipProgressDialog!!.setCornerRadius(16)
        flipProgressDialog!!.setImageSize(200)
        flipProgressDialog!!.setImageMargin(10)
        flipProgressDialog!!.setOrientation("rotationY")
        flipProgressDialog!!.setDuration(600)
        flipProgressDialog!!.setStartAngle(0.0f)
        flipProgressDialog!!.setEndAngle(180.0f)
        flipProgressDialog!!.setMinAlpha(0.0f)
        flipProgressDialog!!.setMaxAlpha(1.0f)
        /*FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
                fcmToken = instanceIdResult.token
                Log.d("TOKEN", fcmToken)
            }*/
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        mobile_til = findViewById(R.id.mobile_til)
        mobile_edt = findViewById(R.id.mobile_edt)
        mobile_edt?.setTypeface(regular)
        login_btn = findViewById(R.id.login_btn)
        login_btn?.setTypeface(bold)
        login_btn?.setOnClickListener(this)
        //serviceType
        getServiceType()
    }
    //flipProgressDialog.dismiss();
    //flipProgressDialog.show(getFragmentManager(), "");
    private fun getServiceType() {
        if (checkInternet) {
            //flipProgressDialog.show(getFragmentManager(), "");
            val url: String = AppUrls.BASE_URL + AppUrls.SERVICETYPE
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener { response ->
                    try {
                        val jsonObject = JSONObject(response)
                        val status = jsonObject.getString("status")
                        if (status.equals("2000", ignoreCase = true)) {
                            //flipProgressDialog.dismiss();
                            val jsonObject1 = jsonObject.getJSONObject("data")
                            val jsonArray = jsonObject1.getJSONArray("files")
                            val jsonArray1 = jsonObject1.getJSONArray("services")
                            val sliderModels: MutableList<SliderModel> = ArrayList<SliderModel>()
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject2 = jsonArray.getJSONObject(i)
                                val serviceTypeImageModel = ServiceTypeImageModel()
                                serviceTypeImageModel.desc = jsonObject2.optString("desc")
                                serviceTypeImageModel.fileId = AppUrls.IMAGE_URL.toString() + jsonObject2.getString("fileId")
                                val sliderModel = SliderModel()
                                user_image = AppUrls.IMAGE_URL + jsonObject2.getString("fileId")
                                sliderModel.imageUrl = user_image
                                sliderModel.description = jsonObject1.optString("desc")
                                sliderModels.add(sliderModel)
                            }
                            adapter?.renewItems(sliderModels)
                        } else {
                            val snackbar = Snackbar.make(
                                window.decorView,
                                "Invalid Data..!",
                                Snackbar.LENGTH_LONG
                            )
                            snackbar.show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error -> error.message })
            val requestQueue = Volley.newRequestQueue(this@LoginActivity)
            requestQueue.add(stringRequest)
        } else {
            val snackbar = Snackbar.make(
                window.decorView,
                "Check Internet Connection",
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }

    override fun onClick(v: View) {
        if (v === login_btn) {
            login()
        }
    }

    private fun validateMobile(): Boolean {
        var result = true
        if (mobile!!.length != 10) {
            mobile_til!!.error = "Invalid Mobile Number"
            result = false
        } else {
            mobile_til!!.error = null
        }
        return result
    }

    private fun login() {
        mobile = mobile_edt!!.text.toString()
        if (validateMobile()) {
            if (checkInternet) {
                flipProgressDialog!!.show(fragmentManager, "")
                val url: String = AppUrls.BASE_URL + AppUrls.LOGIN.toString() + mobile
                Log.d("LOGINURL", url)
                val stringRequest =
                    StringRequest(
                        Request.Method.GET, url,
                        Response.Listener { response ->
                            try {
                                val jsonObject = JSONObject(response)
                                Log.d("LOGIN", response)
                                val status = jsonObject.getString("status")
                                val message = jsonObject.optString("message")
                                if (status.equals("2000", ignoreCase = true)) {
                                    flipProgressDialog!!.dismiss()
                                    otpDialog()
                                } else {
                                    flipProgressDialog!!.dismiss()
                                    GlobalCalls.showToast(message, this@LoginActivity)
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { error ->
                            flipProgressDialog!!.dismiss()
                            if (error == null || error.networkResponse == null) {
                                return@ErrorListener
                            }
                            try {
                                val body =
                                    String(error.networkResponse.data, Charset.forName("UTF-8"))
                                val jsonObject = JSONObject(body)
                                GlobalCalls.showToast(
                                    jsonObject.optString("message"),
                                    this@LoginActivity
                                )
                            } catch (e: Exception) {
                                GlobalCalls.showToast("You Are Not Registered..!", this@LoginActivity)
                            }

                            //GlobalCalls.showToast("You Are Not registered..!",LoginActivity.this);
                        })
                val requestQueue = Volley.newRequestQueue(this@LoginActivity)
                requestQueue.add(stringRequest)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun otpDialog() {
        val builder =
            AlertDialog.Builder(this@LoginActivity)
        builder.setCancelable(false)
        val inflater = this@LoginActivity.layoutInflater
        val dialog_layout = inflater.inflate(R.layout.login_otp_dialog, null)
        val otp_txt = dialog_layout.findViewById<TextView>(R.id.otp_txt)
        otp_txt.setTypeface(bold)
        otpTextView = dialog_layout.findViewById(R.id.otpTextView)
        val resend_txt = dialog_layout.findViewById<TextView>(R.id.resend_txt)
        resend_txt.setTypeface(regular)
        resend_txt.paintFlags = resend_txt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        smsVerifyCatcher = SmsVerifyCatcher(this, OnSmsCatchListener { message ->
            otp = parseCode(message)
            otpTextView?.setOTP(otp)
        })
        smsVerifyCatcher!!.onStart()
        resend_txt.setOnClickListener {
            flipProgressDialog!!.show(fragmentManager, "")
            //String url = AppUrls.BASE_URL + AppUrls.LOGIN + mobile;
            val URL: String =
                AppUrls.BASE_URL + AppUrls.CALL_OTP.toString() + mobile.toString() + "&retrytype=text"
            mobile = mobile_edt!!.text.toString()
            otpTextView?.setOTP("")
            if (validateMobile()) {
                if (checkInternet) {
                    //flipProgressDialog.show(getFragmentManager(), "");
                    val stringRequest =
                        StringRequest(
                            Request.Method.GET, URL,
                            Response.Listener { response ->
                                try {
                                    val jsonObject = JSONObject(response)
                                    Log.d("LOGIN", response)
                                    val status =
                                        jsonObject.getString("status")
                                    val message =
                                        jsonObject.getString("message")
                                    if (status.equals("2000", ignoreCase = true)) {
                                        flipProgressDialog!!.dismiss()
                                    } else {
                                        flipProgressDialog!!.dismiss()
                                        GlobalCalls.showToast(message, this@LoginActivity)
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            },
                            Response.ErrorListener { error ->
                                if (error == null || error.networkResponse == null) {
                                    return@ErrorListener
                                }
                                try {
                                    val body =
                                        String(error.networkResponse.data, Charset.forName("UTF-8"))
                                    val jsonObject = JSONObject(body)
                                    GlobalCalls.showToast(
                                        jsonObject.optString("message"),
                                        this@LoginActivity
                                    )
                                } catch (e: Exception) {
                                    GlobalCalls.showToast(
                                        "Invalid Details..!",
                                        this@LoginActivity
                                    )
                                }
                            })
                    val requestQueue = Volley.newRequestQueue(this@LoginActivity)
                    requestQueue.add(stringRequest)
                } else {
                    val snackbar = Snackbar.make(
                        window.decorView,
                        "Check Internet Connection",
                        Snackbar.LENGTH_LONG
                    )
                    snackbar.show()
                }
            }
        }
        val verify_btn =
            dialog_layout.findViewById<Button>(R.id.verify_btn)
        verify_btn.setTypeface(bold)
        verify_btn.setOnClickListener {
            flipProgressDialog!!.show(fragmentManager, "")
            otp = otpTextView?.getOTP()
            //String url = AppUrls.BASE_URL + AppUrls.VERIFYOTP + mobile + "&otp=" + otp;
            val url: String =
                AppUrls.BASE_URL + AppUrls.VERIFYOTP.toString() + mobile.toString() + "&otp=" + otp
            Log.d("VERIFY", url)
            val otp: String = otpTextView?.getOTP()!!
            if (validate()) {
                if (checkInternet) {
                    val stringRequest =
                        StringRequest(
                            Request.Method.GET, url,
                            Response.Listener { response ->
                                try {
                                    val jsonObject1 = JSONObject(response)
                                    flipProgressDialog!!.dismiss()
                                    Log.d("OTPRESP", response)
                                    userId = jsonObject1.optInt("userId")
                                    firstName = jsonObject1.optString("firstName")
                                    username = jsonObject1.optString("username")
                                    locked = jsonObject1.optBoolean("locked")
                                    enabled = jsonObject1.optBoolean("enabled")
                                    expired = jsonObject1.optBoolean("expired")
                                    authority = jsonObject1.optString("authority")
                                    email = jsonObject1.optString("email")
                                    mobile = jsonObject1.optString("mobile")
                                    emailVerified = jsonObject1.optBoolean("emailVerified")
                                    mobileVerified =
                                        jsonObject1.optBoolean("mobileVerified")
                                    createdTime = jsonObject1.optString("createdTime")
                                    modifiedTime = jsonObject1.optString("modifiedTime")
                                    accessToken = jsonObject1.optString("accessToken")
                                    mediaSecret = jsonObject1.optString("mediaSecret")
                                    Log.d(
                                        "LoginDetails",
                                        """
                                        ${userId.toString()}
                                        $firstName
                                        $username
                                        $locked
                                        $enabled
                                        $expired
                                        $authority
                                        $email
                                        $mobile
                                        $emailVerified
                                        $mobileVerified
                                        $createdTime
                                        $modifiedTime
                                        $accessToken
                                        $mediaSecret
                                        """.trimIndent()
                                    )
                                    if (authority.equals(
                                            "ROLE_SERVICE_CENTER",
                                            ignoreCase = true
                                        )
                                    ) {
                                        userSessionManager?.createUserLoginSession(
                                            firstName,
                                            username,
                                            email,
                                            mobile,
                                            accessToken,
                                            mediaSecret
                                        )
                                        sendFCM()
                                        val intent = Intent(
                                            this@LoginActivity,
                                            MainActivity::class.java
                                        )
                                        startActivity(intent)
                                    } else {
                                        Toast.makeText(
                                            this@LoginActivity,
                                            "Authentication Failed..!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            },
                            Response.ErrorListener { error ->
                                flipProgressDialog!!.dismiss()
                                otpTextView?.showError()
                                if (error == null || error.networkResponse == null) {
                                    return@ErrorListener
                                }
                                try {
                                    val body =
                                        String(error.networkResponse.data, Charset.forName("UTF-8"))
                                    val jsonObject = JSONObject(body)
                                    GlobalCalls.showToast(
                                        jsonObject.optString("message"),
                                        this@LoginActivity
                                    )
                                } catch (e: Exception) {
                                    GlobalCalls.showToast(
                                        "Invalid Otp..!",
                                        this@LoginActivity
                                    )
                                }
                            })
                    stringRequest.retryPolicy = DefaultRetryPolicy(
                        500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
                    val requestQueue = Volley.newRequestQueue(this@LoginActivity)
                    requestQueue.add(stringRequest)
                } else {
                    val snackbar = Snackbar.make(
                        window.decorView,
                        "Check Internet Connection",
                        Snackbar.LENGTH_LONG
                    )
                    snackbar.show()
                }
            }
        }
        val call_txt = dialog_layout.findViewById<TextView>(R.id.call_txt)
        call_txt.visibility = View.VISIBLE
        val call_btn =
            dialog_layout.findViewById<Button>(R.id.call_btn)
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                call_txt.text = "Seconds Remaining : " + millisUntilFinished / 1000
            }

            override fun onFinish() {
                call_txt.text = "done!"
                call_txt.visibility = View.GONE
                resend_txt.visibility = View.VISIBLE
                call_btn.visibility = View.VISIBLE
                call_btn.setOnClickListener {
                    call_btn.setTextColor(
                        ContextCompat.getColor(
                            this@LoginActivity,
                            R.color.white
                        )
                    )
                    call_btn.setBackgroundResource(R.drawable.rounded_orange_border)
                    flipProgressDialog!!.show(fragmentManager, "")
                    val stringRequest =
                        StringRequest(
                            Request.Method.GET,
                            AppUrls.BASE_URL + AppUrls.CALL_OTP.toString() + mobile.toString() + "&retrytype=voice",
                            Response.Listener { response ->
                                try {
                                    val jsonObject = JSONObject(response)
                                    Log.d("CALL", response)

                                    /*call_txt.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.black));
                                                                                    call_btn.setBackgroundResource(R.drawable.searchview_boarder);*/
                                    val status =
                                        jsonObject.getString("status")
                                    val message =
                                        jsonObject.getString("message")
                                    if (status.equals("2000", ignoreCase = true)) {
                                        flipProgressDialog!!.dismiss()
                                    } else {
                                        flipProgressDialog!!.dismiss()
                                        GlobalCalls.showToast(message, this@LoginActivity)
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            },
                            Response.ErrorListener { error ->
                                if (error == null || error.networkResponse == null) {
                                    return@ErrorListener
                                }
                                try {
                                    val body =
                                        String(error.networkResponse.data, Charset.forName("UTF-8"))
                                    val jsonObject = JSONObject(body)
                                    GlobalCalls.showToast(
                                        jsonObject.optString("message"),
                                        this@LoginActivity
                                    )
                                } catch (e: Exception) {
                                    GlobalCalls.showToast(
                                        "Invalid Details..!",
                                        this@LoginActivity
                                    )
                                }
                            })
                    stringRequest.retryPolicy = DefaultRetryPolicy(
                        500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
                    val requestQueue = Volley.newRequestQueue(this@LoginActivity)
                    requestQueue.add(stringRequest)
                }
            }
        }.start()
        builder.setView(dialog_layout)
            .setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.dismiss() }
        dialog = builder.create()
        dialog!!.show()
    }

    private fun validate(): Boolean {
        var result = true
        //String otp = otpTextView.getText().toString().trim();
        otp = otpTextView?.getOTP()
        Log.d("OTP", otp)
        if (otp == null || otp == "" || otp!!.length != 6) {
            //otpTextView.setError("Invalid OTP");
            otpTextView?.showError()
            result = false
        }
        return result
    }

    private fun parseCode(message: String): String {
        val p = Pattern.compile("\\b\\d{6}\\b")
        val m = p.matcher(message)
        var code = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    private fun sendFCM() {
        val url: String = AppUrls.BASE_URL + AppUrls.FCM
        val jsonObj = JSONObject()
        Log.d("TOKENNNN", "" + fcmToken)
        try {
            jsonObj.put("platformId", "1")
            jsonObj.put("pushRegId", fcmToken)
            jsonObj.put("imei", imei)
            jsonObj.put("roleType", "2")
            jsonObj.put("userId", userId)
        } catch (e: Exception) {
        }
        Log.d("JsonObj", jsonObj.toString())
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObj,
            Response.Listener { response ->
                Log.d("FCMStatus", response.toString())
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    Log.d(
                        "Status", """
     $status
     $message
     """.trimIndent()
                    )
                    if (status.equals("2000", ignoreCase = true)) {

                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Log.d("ResError", e.message)
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace()

                //Log.d("EROORRR", error.getMessage());
            }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@LoginActivity)
        requestQueue.add(request)
    }

    override fun onBackPressed() {
        if (exit) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            moveTaskToBack(true)
        } else {
            GlobalCalls.showToast("Press Back again to Exit.!", this@LoginActivity)
            exit = true
            Handler().postDelayed({ exit = false }, 2 * 1000.toLong())
        }
    }
}