package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.MyAccountActivity
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MyAccountActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var edit_img: ImageView? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var toolbar_title: TextView? = null
    var name_txt: TextView? = null
    var email_txt: TextView? = null
    var mobile_txt: TextView? = null
    var userId: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var username: String? = null
    var authority: String? = null
    var email: String? = null
    var mobile: String? = null
    var emailVerified: String? = null
    var mobileVerified: String? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        mobile = userDetails?.get(UserSessionManager.USER_MOBILE)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        edit_img = findViewById(R.id.edit_img)
        edit_img?.setOnClickListener(this)
        name_txt = findViewById(R.id.name_txt)
        name_txt?.setTypeface(regular)
        email_txt = findViewById(R.id.email_txt)
        email_txt?.setTypeface(regular)
        mobile_txt = findViewById(R.id.mobile_txt)
        mobile_txt?.setTypeface(regular)
        mobile_txt?.setText(mobile)
        profile
    }

    private val profile: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.PROFILE
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        try {
                            val jsonObject = JSONObject(response)
                            userId = jsonObject.optString("userId")
                            firstName = jsonObject.optString("firstName")
                            lastName = jsonObject.optString("lastName")
                            username = jsonObject.optString("username")
                            authority = jsonObject.optString("authority")
                            email = jsonObject.optString("email")
                            mobile = jsonObject.optString("mobile")
                            emailVerified = jsonObject.optString("emailVerified")
                            mobileVerified = jsonObject.optString("mobileVerified")
                            name_txt!!.text = firstName
                            email_txt!!.text = email
                            mobile_txt!!.text = mobile
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@MyAccountActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@MyAccountActivity, SettingActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === edit_img) {
            if (checkInternet) {
                val intent = Intent(this@MyAccountActivity, EditProfileActivity::class.java)
                intent.putExtra("firstName", firstName)
                intent.putExtra("lastName", lastName)
                intent.putExtra("email", email)
                intent.putExtra("authority", authority)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@MyAccountActivity, SettingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }
}