package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.adapters.DropOffInspectionsAdapter
import com.example.gaadi360partnerkotlin.models.DropOffInspectionsModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.function.Predicate

class DropOffInspectionsActivity : AppCompatActivity(),
    View.OnClickListener {
    var close: ImageView? = null
    var no_history_img: ImageView? = null
    var toolbar_title: TextView? = null
    var no_history_txt: TextView? = null
    var booking_txt: TextView? = null
    private var checkInternet = false
    var drop_recyclerview: RecyclerView? = null
    var submit_btn: Button? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var bookingId: String? = null

    /*DropOff Inspections*/
    var dropOffInspectionsAdapter: DropOffInspectionsAdapter? = null
    var dropOffInspectionsModels: ArrayList<DropOffInspectionsModel> =
        ArrayList<DropOffInspectionsModel>()
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drop_off_inspections)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        bookingId = intent.getStringExtra("bookingId")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        no_history_img = findViewById(R.id.no_history_img)
        no_history_txt = findViewById(R.id.no_history_txt)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        booking_txt = findViewById(R.id.booking_txt)
        booking_txt?.setText("Booking Id : $bookingId")
        submit_btn = findViewById(R.id.submit_btn)
        submit_btn?.setOnClickListener(this)
        drop_recyclerview = findViewById(R.id.drop_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        drop_recyclerview?.setLayoutManager(layoutManager)
        dropOffInspectionsAdapter = DropOffInspectionsAdapter(
            dropOffInspectionsModels,
            this@DropOffInspectionsActivity,
            R.layout.row_inspection
        )
        dropOffInspections
    }

    private val dropOffInspections: Unit
        private get() {
            val url: String = AppUrls.BASE_URL + AppUrls.DROP_OFF_INSPECTIONS
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener { response ->
                    dropOffInspectionsModels.clear()
                    try {
                        val jsonObject = JSONObject(response)
                        val responceCode = jsonObject.getString("status")
                        if (responceCode == "2000") {
                            val jsonArray = jsonObject.getJSONArray("data")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val im = DropOffInspectionsModel()
                                im.inspectionId=(jsonObject1.getString("inspectionId"))
                                im.inspectionName=(jsonObject1.getString("inspectionName"))
                                im.fileId=(AppUrls.IMAGE_URL.toString() + jsonObject1.getString("fileId"))
                                dropOffInspectionsModels.add(im)
                            }
                            drop_recyclerview!!.adapter = dropOffInspectionsAdapter
                            dropOffInspectionsAdapter?.notifyDataSetChanged()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            )
            val requestQueue = Volley.newRequestQueue(this@DropOffInspectionsActivity)
            requestQueue.add(stringRequest)
        }

    override fun onClick(v: View) {
        if (v === close) {
            val intent =
                Intent(this@DropOffInspectionsActivity, BookingDetailActivity::class.java)
            intent.putExtra("activity", "AddonServicesActivity")
            intent.putExtra("bookingId", bookingId)
            startActivity(intent)
        }
        if (v === submit_btn) {
            var status = false
            if (Build.VERSION.SDK_INT >= VERSION_CODES.N) {
                status = dropOffInspectionsModels.stream()
                    .anyMatch(Predicate<DropOffInspectionsModel> { t: DropOffInspectionsModel ->
                        t.status.equals("0")
                    })
            }
            if (!status) {
                delivered()
            } else {
                GlobalCalls.showToast(
                    "Please Select All Inspections..!",
                    this@DropOffInspectionsActivity
                )
            }
        }
    }

    private fun delivered() {
        val url: String =
            AppUrls.BASE_URL + AppUrls.DELIVERED.toString() + bookingId.toString() + "/delivered"
        val jsonObject = JSONObject()
        try {
            jsonObject.put("bookingId", bookingId)
            val jArray = JSONArray()
            for (i in dropOffInspectionsModels.indices) {
                val jsonObject1 = JSONObject()
                jsonObject1.put("inspectionId", dropOffInspectionsModels[i].inspectionId)
                jsonObject1.put("state", dropOffInspectionsModels[i].status)
                jsonObject1.put(
                    "inspectionName",
                    dropOffInspectionsModels[i].inspectionName
                )
                jArray.put(jsonObject1)
            }
            jsonObject.put("dropOffInspections", jArray)
            Log.d("ARRAYDATA", "" + jArray)
            Log.d("JAYYYY", "" + jsonObject)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObject,
            Response.Listener { response ->
                Log.d("SuccessResp", response.toString())
                try {
                    val jsonObj = JSONObject(response.toString())
                    val status = jsonObj.getString("status")
                    val message = jsonObj.getString("message")
                    val code = jsonObj.getString("code")
                    GlobalCalls.showToast(message, this@DropOffInspectionsActivity)
                    val intent =
                        Intent(this@DropOffInspectionsActivity, DeliveredActivity::class.java)
                    startActivity(intent)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error: VolleyError -> error.printStackTrace() }
        ) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@DropOffInspectionsActivity)
        requestQueue.add(request)
    }

    override fun onBackPressed() {
        val intent = Intent(this@DropOffInspectionsActivity, BookingDetailActivity::class.java)
        intent.putExtra("activity", "AddonServicesActivity")
        intent.putExtra("bookingId", bookingId)
        startActivity(intent)
    }
}