package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.example.gaadi360partnerkotlin.MainActivity
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.SettingActivity
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import java.lang.reflect.Field

class SettingActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var toolbar_title: TextView? = null
    var acc_txt: TextView? = null
    var about_txt: TextView? = null
    var t_txt: TextView? = null
    var help_txt: TextView? = null
    var privacy_txt: TextView? = null
    var can_txt: TextView? = null
    var logout_txt: TextView? = null
    var version_txt: TextView? = null
    var my_accountRL: RelativeLayout? = null
    var aboutUsRL: RelativeLayout? = null
    var tcRL: RelativeLayout? = null
    var help_centerRL: RelativeLayout? = null
    var privacy_policyRL: RelativeLayout? = null
    var refund_policyRL: RelativeLayout? = null
    var logoutRL: RelativeLayout? = null
    var userSessionManager: UserSessionManager? = null
    var sweetAlertDialog: SweetAlertDialog? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    var versionName: String? = null
    var versionCode = 0
    var dialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        acc_txt = findViewById(R.id.acc_txt)
        acc_txt?.setTypeface(regular)
        about_txt = findViewById(R.id.about_txt)
        about_txt?.setTypeface(regular)
        t_txt = findViewById(R.id.t_txt)
        t_txt?.setTypeface(regular)
        help_txt = findViewById(R.id.help_txt)
        help_txt?.setTypeface(regular)
        privacy_txt = findViewById(R.id.privacy_txt)
        privacy_txt?.setTypeface(regular)
        can_txt = findViewById(R.id.can_txt)
        can_txt?.setTypeface(regular)
        logout_txt = findViewById(R.id.logout_txt)
        logout_txt?.setTypeface(regular)
        my_accountRL = findViewById(R.id.my_accountRL)
        my_accountRL?.setOnClickListener(this)
        aboutUsRL = findViewById(R.id.aboutUsRL)
        aboutUsRL?.setOnClickListener(this)
        tcRL = findViewById(R.id.tcRL)
        tcRL?.setOnClickListener(this)
        help_centerRL = findViewById(R.id.help_centerRL)
        help_centerRL?.setOnClickListener(this)
        privacy_policyRL = findViewById(R.id.privacy_policyRL)
        privacy_policyRL?.setOnClickListener(this)
        refund_policyRL = findViewById(R.id.refund_policyRL)
        refund_policyRL?.setOnClickListener(this)
        logoutRL = findViewById(R.id.logoutRL)
        logoutRL?.setOnClickListener(this)
        try {
            val pInfo =
                packageManager.getPackageInfo(packageName, 0)
            versionName = pInfo.versionName
            versionCode = pInfo.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val builder = StringBuilder()
        builder.append("Android : ").append(Build.VERSION.RELEASE)
        var fields =
            arrayOfNulls<Field>(0)
        if (Build.VERSION.SDK_INT >= VERSION_CODES.DONUT) {
            fields = VERSION_CODES::class.java.fields
        }
        for (field in fields) {
            val fieldName = field!!.name
            var fieldValue = -1
            try {
                fieldValue = field.getInt(Any())
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append("(").append(fieldName).append(")")
                //builder.append("sdk=").append(fieldValue);
            }
        }
        Log.d("OS: ", builder.toString())
        version_txt = findViewById(R.id.version_txt)
        version_txt?.setTypeface(regular)
        version_txt?.setText("Version $versionName - ($builder)")
    }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@SettingActivity, MainActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === my_accountRL) {
            if (checkInternet) {
                val intent = Intent(this@SettingActivity, MyAccountActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === aboutUsRL) {
            if (checkInternet) {
                val intent = Intent(this@SettingActivity, AboutUsActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === tcRL) {
            if (checkInternet) {
                val intent =
                    Intent(this@SettingActivity, TermsAndConditionsActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === help_centerRL) {
            if (checkInternet) {
                val intent = Intent(this@SettingActivity, HelpCenterActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === privacy_policyRL) {
            if (checkInternet) {
                val intent =
                    Intent(this@SettingActivity, PrivacyAndPolicyActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === refund_policyRL) {
            if (checkInternet) {
                val intent =
                    Intent(this@SettingActivity, CancellationAndRefundPolicyActivity::class.java)
                startActivity(intent)
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === logoutRL) {
            if (checkInternet) {
                showLogoutDialog(R.layout.warning_dialog)

                /*sweetAlertDialog = new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setCancelText("No, Stay");
                sweetAlertDialog.setConfirmText("Yes, Log Out");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    userSessionManager.logoutUser();

                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                });

                sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                sweetAlertDialog.show();*/
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun showLogoutDialog(layout: Int) {
        dialogBuilder = AlertDialog.Builder(this@SettingActivity)
        val layoutView = layoutInflater.inflate(layout, null)
        val title_txt = layoutView.findViewById<TextView>(R.id.title_txt)
        title_txt.setTypeface(bold)
        title_txt.text = "Are you sure?"
        val msg_txt = layoutView.findViewById<TextView>(R.id.msg_txt)
        msg_txt.visibility = View.GONE
        val ok_btn =
            layoutView.findViewById<Button>(R.id.ok_btn)
        ok_btn.setTypeface(bold)
        ok_btn.text = "Yes, Log Out"
        val cancel_btn =
            layoutView.findViewById<Button>(R.id.cancel_btn)
        cancel_btn.setTypeface(bold)
        cancel_btn.text = "No, Stay"
        dialogBuilder!!.setView(layoutView)
        alertDialog = dialogBuilder!!.create()
        alertDialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
        alertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
        ok_btn.setOnClickListener {
            alertDialog!!.dismiss()
            userSessionManager?.logoutUser()
            val intent = Intent(this@SettingActivity, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        cancel_btn.setOnClickListener { alertDialog!!.dismiss() }
        alertDialog!!.setCancelable(false)
    }

    override fun onBackPressed() {
        val intent = Intent(this@SettingActivity, MainActivity::class.java)
        startActivity(intent)
    }
}