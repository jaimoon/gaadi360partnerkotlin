package com.example.gaadi360partnerkotlin.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.adapters.AddonServicesAdapter
import com.example.gaadi360partnerkotlin.models.AddonServicesModel
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls.showToast
import com.example.gaadi360partnerkotlin.utils.NetworkChecking.isConnected
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import com.taishi.flipprogressdialog.FlipProgressDialog
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class AddonServicesActivity : AppCompatActivity(), View.OnClickListener {
    var close: ImageView? = null
    var no_history_img: ImageView? = null
    var toolbar_title: TextView? = null
    var no_history_txt: TextView? = null
    var price_txt: TextView? = null
    private var checkInternet = false
    var addon_recyclerview: RecyclerView? = null
    var view1: View? = null
    var view2: View? = null
    var submit_btn: Button? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var addonCost: String? = null
    lateinit var serviceIds: String
    var jsonObj: String? = null
    var addon_search: SearchView? = null

    /*History*/
    var addonServicesAdapter: AddonServicesAdapter? = null
    var addonServicesModels = ArrayList<AddonServicesModel>()
    var regular: Typeface? = null
    var bold: Typeface? = null
    var bookingId: String? = null
    var serviceCenterId: String? = null
    var userId: String? = null
    var serviceStr: String? = null
    var addOnStr: String? = null
    var alreadyServiceArray: JSONArray? = null
    var alreadyAddedArray: JSONArray? = null

    /*ProgressDialog*/
    var imageList: MutableList<Int> = ArrayList()
    var flipProgressDialog: FlipProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addon_services)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails =
            userSessionManager!!.userDetails
        accessToken = userDetails!![UserSessionManager.KEY_ACCSES]
        serviceCenterId = intent.getStringExtra("serviceCenterId")
        bookingId = intent.getStringExtra("bookingId")
        userId = intent.getStringExtra("userId")
        serviceIds = intent.getStringExtra("serviceIds")
        jsonObj = intent.getStringExtra("jsonObj")
        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject(jsonObj)
            if (jsonObject.has("services")) {
                alreadyServiceArray = jsonObject.getJSONArray("services")
                Log.d("JSONARRAY1", alreadyServiceArray.toString())
            }
            if (jsonObject.has("bookingAddonServices")) {
                alreadyAddedArray = jsonObject.getJSONArray("bookingAddonServices")
                Log.d("JSONARRAY2", alreadyAddedArray.toString())
            }
            Log.d("JSONARRAY3", alreadyAddedArray.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        imageList.add(R.mipmap.ic_launcher)
        flipProgressDialog = FlipProgressDialog()
        flipProgressDialog!!.setImageList(imageList)
        flipProgressDialog!!.setCanceledOnTouchOutside(true)
        flipProgressDialog!!.setDimAmount(0.8f) //0.0f
        flipProgressDialog!!.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        flipProgressDialog!!.setBackgroundAlpha(0.2f)
        flipProgressDialog!!.setBorderStroke(0)
        flipProgressDialog!!.setBorderColor(-1)
        flipProgressDialog!!.setCornerRadius(16)
        flipProgressDialog!!.setImageSize(200)
        flipProgressDialog!!.setImageMargin(10)
        flipProgressDialog!!.setOrientation("rotationY")
        flipProgressDialog!!.setDuration(600)
        flipProgressDialog!!.setStartAngle(0.0f)
        flipProgressDialog!!.setEndAngle(180.0f)
        flipProgressDialog!!.setMinAlpha(0.0f)
        flipProgressDialog!!.setMaxAlpha(1.0f)
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        no_history_img = findViewById(R.id.no_history_img)
        no_history_txt = findViewById(R.id.no_history_txt)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        price_txt = findViewById(R.id.price_txt)
        price_txt?.setTypeface(bold)
        view1 = findViewById(R.id.view1)
        view2 = findViewById(R.id.view2)
        submit_btn = findViewById(R.id.submit_btn)
        submit_btn?.setOnClickListener(this)
        addon_recyclerview = findViewById(R.id.addon_recyclerview)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        addon_recyclerview?.setLayoutManager(layoutManager)
        addonServicesAdapter = AddonServicesAdapter(serviceIds, addonServicesModels, this@AddonServicesActivity, R.layout.row_addon_services)
        addons
        totalPrice
        addon_search = findViewById(R.id.addon_search)
        val searchEditText = addon_search?.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        addon_search?.setOnClickListener(View.OnClickListener { v: View? ->
            addon_search?.setIconified(
                false
            )
        })
        addon_search?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                addonServicesAdapter!!.filter?.filter(query)
                addonServicesAdapter!!.notifyDataSetChanged()
                return false
            }
        })
    }

    val addons: Unit
        get() {
            val url: String =
                AppUrls.BASE_URL + AppUrls.ADDON_SERVICES.toString() + serviceCenterId.toString() + "/services"
            val stringRequest: StringRequest =
                object : StringRequest(
                    Method.GET,
                    url,
                    Response.Listener { response ->
                        addonServicesModels.clear()
                        try {
                            val jsonArray = JSONArray(response)
                            if (jsonArray.length() != 0) {
                                addon_search!!.visibility = View.VISIBLE
                                no_history_img!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.GONE
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject = jsonArray.getJSONObject(i)
                                    val asm = AddonServicesModel()
                                    asm.serviceId = jsonObject.optString("serviceId")
                                    asm.serviceName = jsonObject.optString("serviceName")
                                    asm.deleted = jsonObject.optString("deleted")
                                    asm.createdTime = jsonObject.optString("createdTime")
                                    asm.modifiedTime = jsonObject.optString("modifiedTime")
                                    asm.cost = jsonObject.optString("cost")
                                    for (k in 0 until alreadyServiceArray!!.length()) {
                                        val jsonObject2 =
                                            alreadyServiceArray!!.getJSONObject(k)
                                        val serviceId =
                                            jsonObject2.optString("serviceId")
                                        val cost = jsonObject2.optString("cost")
                                        if (jsonObject.optString("serviceId")
                                                .equals(serviceId, ignoreCase = true)
                                        ) {
                                            asm.cost = cost
                                        }
                                        Log.d("ALREADY", "$serviceId->$cost")
                                    }
                                    for (j in 0 until alreadyAddedArray!!.length()) {
                                        val jsonObject1 =
                                            alreadyAddedArray!!.getJSONObject(j)
                                        val serviceId =
                                            jsonObject1.optString("serviceId")
                                        val cost = jsonObject1.optString("cost")
                                        val status = jsonObject1.optString("status")
                                        if (jsonObject.optString("serviceId")
                                                .equals(serviceId, ignoreCase = true)
                                        ) {
                                            asm.cost = cost
                                            if (!status.equals("2", ignoreCase = true)) {
                                                asm.isAddonService = true
                                            }
                                        }
                                        Log.d("ALREADY", "$serviceId->$cost")
                                    }
                                    addonServicesModels.add(asm)
                                }
                                addon_recyclerview!!.adapter = addonServicesAdapter
                                addonServicesAdapter!!.notifyDataSetChanged()
                            } else {
                                addon_search!!.visibility = View.GONE
                                no_history_txt!!.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        if (error is TimeoutError || error is NoConnectionError) {
                        } else if (error is AuthFailureError) {
                        } else if (error is ServerError) {
                        } else if (error is NetworkError) {
                        } else if (error is ParseError) {
                        }
                    }
                ) {
                    override fun getHeaders(): Map<String, String> {
                        val headers: MutableMap<String, String> =
                            HashMap()
                        headers["Authorization"] = "Bearer $accessToken"
                        return headers
                    }
                }
            val requestQueue = Volley.newRequestQueue(this@AddonServicesActivity)
            requestQueue.add(stringRequest)
        }

    val totalPrice: Unit
        get() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                addonCost = "" + addonServicesModels.stream()
                    .filter { t: AddonServicesModel ->
                        t.status.equals(
                            "1",
                            ignoreCase = true
                        )
                    }.mapToDouble { t: AddonServicesModel ->
                        java.lang.Double.valueOf(
                            t.cost!!
                        )
                    }.sum()
            }
            view1!!.visibility = View.VISIBLE
            view2!!.visibility = View.VISIBLE
            price_txt!!.visibility = View.VISIBLE
            Log.d("ADDONPRICE", addonCost)
            price_txt!!.text = "Final Price : \u20B9 $addonCost"
        }

    override fun onClick(v: View) {
        if (v === close) {
            val intent =
                Intent(this@AddonServicesActivity, BookingDetailActivity::class.java)
            intent.putExtra("activity", "AddonServicesActivity")
            intent.putExtra("bookingId", bookingId)
            startActivity(intent)
        }
        if (v === submit_btn) {

            /*long count = 0L;
            String[] split = serviceIds.split(",");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                List<String> validate = Arrays.stream(split).filter(t -> !t.equalsIgnoreCase("")).collect(Collectors.toList());
                count = addonServicesModels.stream().filter(t -> t.getStatus().equalsIgnoreCase("1")).count();

                if (Long.valueOf(validate.size()) < count) {
                    sendAddon();
                } else {
                    GlobalCalls.showToast("Please Add Services..!", AddonServicesActivity.this);
                }
            }*/
            sendAddon()
        }
    }

    private fun sendAddon() {
        flipProgressDialog!!.show(fragmentManager, "")
        if (checkInternet) {
            val url: String = AppUrls.BASE_URL + AppUrls.SEND_ADDON
            val jsonObject = JSONObject()
            try {
                jsonObject.put("bookingId", bookingId)
                jsonObject.put("serviceCenterId", serviceCenterId)
                jsonObject.put("finalPrice", addonCost)
                jsonObject.put("userId", userId)
                val jArray = JSONArray()
                for (i in addonServicesModels.indices) {
                    if (addonServicesModels[i].status.equals("1", ignoreCase = true)) {
                        val jsonObject1 = JSONObject()
                        jsonObject1.put("serviceId", addonServicesModels[i].serviceId)
                        jsonObject1.put("serviceName", addonServicesModels[i].serviceName)
                        jsonObject1.put("cost", addonServicesModels[i].cost)
                        jArray.put(jsonObject1)
                    }
                }
                jsonObject.put("services", jArray)
                Log.d("ARRAYDATA", "" + jArray)
                Log.d("JAYYYY", "" + jsonObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            val request: JsonObjectRequest = object : JsonObjectRequest(
                Method.POST,
                url,
                jsonObject,
                Response.Listener { response ->
                    Log.d("SuccessResp", response.toString())
                    try {
                        val jsonObj = JSONObject(response.toString())
                        val status = jsonObj.getString("status")
                        val message = jsonObj.getString("message")
                        val code = jsonObj.getString("code")
                        flipProgressDialog!!.dismiss()
                        val toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                        //GlobalCalls.showToast(message, this@AddonServicesActivity)
                        val intent = Intent(
                            this@AddonServicesActivity,
                            BookingDetailActivity::class.java
                        )
                        intent.putExtra("activity", "AddonServicesActivity")
                        intent.putExtra("bookingId", bookingId)
                        startActivity(intent)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error: VolleyError ->
                    error.printStackTrace()
                    flipProgressDialog!!.dismiss()
                    if (error is TimeoutError || error is NoConnectionError) {
                    } else if (error is AuthFailureError) {
                    } else if (error is ServerError) {
                    } else if (error is NetworkError) {
                    } else if (error is ParseError) {
                    }
                }
            ) {
                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> =
                        HashMap()
                    headers["Authorization"] = "Bearer $accessToken"
                    return headers
                }
            }
            request.retryPolicy = DefaultRetryPolicy(
                500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            val requestQueue = Volley.newRequestQueue(this@AddonServicesActivity)
            requestQueue.add(request)
        } else {
            val snackbar = Snackbar.make(
                window.decorView,
                "Check Internet Connection",
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@AddonServicesActivity, BookingDetailActivity::class.java)
        intent.putExtra("activity", "AddonServicesActivity")
        intent.putExtra("bookingId", bookingId)
        startActivity(intent)
    }
}