package com.example.gaadi360partnerkotlin.activities

import android.Manifest.permission
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.ComponentName
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.Typeface
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.gaadi360partnerkotlin.R
import com.example.gaadi360partnerkotlin.activities.EditProfileActivity
import com.example.gaadi360partnerkotlin.utils.AppUrls
import com.example.gaadi360partnerkotlin.utils.GlobalCalls
import com.example.gaadi360partnerkotlin.utils.NetworkChecking
import com.example.gaadi360partnerkotlin.utils.UserSessionManager
import com.google.android.material.snackbar.Snackbar
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*

class EditProfileActivity : AppCompatActivity(), View.OnClickListener {
    private var checkInternet = false
    var close: ImageView? = null
    var edit_img: ImageView? = null
    var toolbar_title: TextView? = null
    var userSessionManager: UserSessionManager? = null
    var accessToken: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var authority: String? = null
    var f_name_edt: EditText? = null
    var l_name_edt: EditText? = null
    var email_edt: EditText? = null
    var update_btn: Button? = null
    var picUri: Uri? = null
    var myBitmap: Bitmap? = null
    var getImage: File? = null
    private var permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected =
        ArrayList<String>()
    private val permissions =
        ArrayList<String>()
    var croppedImageView: CircleImageView? = null
    var fileId: String? = null
    var mimeType: String? = null
    var fileName: String? = null
    var filePath: String? = null
    var fileSize: String? = null
    var checksum: String? = null
    var createdTime: String? = null
    var modifiedTime: String? = null
    var regular: Typeface? = null
    var bold: Typeface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        regular = Typeface.createFromAsset(assets, "proxima_nova_regular.otf")
        bold = Typeface.createFromAsset(assets, "proxima_nova_bold.otf")
        checkInternet = NetworkChecking.isConnected(this)
        userSessionManager = UserSessionManager(this)
        val userDetails: HashMap<String?, String?>? = userSessionManager?.userDetails
        accessToken = userDetails?.get(UserSessionManager.KEY_ACCSES)
        firstName = intent.getStringExtra("firstName")
        lastName = intent.getStringExtra("lastName")
        email = intent.getStringExtra("email")
        authority = intent.getStringExtra("authority")
        toolbar_title = findViewById(R.id.toolbar_title)
        toolbar_title?.setTypeface(bold)
        close = findViewById(R.id.close)
        close?.setOnClickListener(this)
        croppedImageView =
            findViewById(R.id.img_profile)
        edit_img = findViewById(R.id.edit_img)
        edit_img?.setOnClickListener(this)
        f_name_edt = findViewById(R.id.f_name_edt)
        f_name_edt?.setText(firstName)
        l_name_edt = findViewById(R.id.l_name_edt)
        l_name_edt?.setText(lastName)
        email_edt = findViewById(R.id.email_edt)
        email_edt?.setText(email)
        update_btn = findViewById(R.id.update_btn)
        update_btn?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v === close) {
            if (checkInternet) {
                val intent = Intent(this@EditProfileActivity, MyAccountActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === edit_img) {
            if (checkInternet) {
                permissions.add(permission.CAMERA)
                permissionsToRequest = findUnAskedPermissions(permissions)
                if (Build.VERSION.SDK_INT >= VERSION_CODES.M) {
                    if (permissionsToRequest!!.size > 0) requestPermissions(
                        permissionsToRequest!!.toTypedArray(),
                        ALL_PERMISSIONS_RESULT
                    )
                }
                startActivityForResult(pickImageChooserIntent, 200)

                //updateProfilePic();
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
        if (v === update_btn) {
            if (checkInternet) {
                updateProfile()
            } else {
                val snackbar = Snackbar.make(
                    window.decorView,
                    "Check Internet Connection",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
            }
        }
    }

    private fun updateProfilePic() {
        val stringRequest: StringRequest = object :
            StringRequest(
                Method.POST,
                AppUrls.BASE_URL + AppUrls.EDIT_PROFILE_PIC,
                Response.Listener { response ->
                    try {
                        val jsonObject = JSONObject(response)
                        fileId = jsonObject.getString("fileId")
                        mimeType = jsonObject.getString("mimeType")
                        fileName = jsonObject.getString("fileName")
                        filePath = jsonObject.getString("filePath")
                        fileSize = jsonObject.getString("fileSize")
                        checksum = jsonObject.getString("checksum")
                        createdTime = jsonObject.getString("createdTime")
                        modifiedTime = jsonObject.getString("modifiedTime")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error -> error.message }) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["file"] = getImage.toString()
                return params
            }
        }
        stringRequest.retryPolicy = DefaultRetryPolicy(
            500000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        val requestQueue = Volley.newRequestQueue(this@EditProfileActivity)
        requestQueue.add(stringRequest)
    }

    val pickImageChooserIntent: Intent
        get() {
            val outputFileUri = captureImageOutputUri
            val allIntents: MutableList<Intent> = ArrayList()
            val packageManager = packageManager
            val captureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val listCam =
                packageManager.queryIntentActivities(captureIntent, 0)
            for (res in listCam) {
                val intent = Intent(captureIntent)
                intent.component =
                    ComponentName(res.activityInfo.packageName, res.activityInfo.name)
                intent.setPackage(res.activityInfo.packageName)
                if (outputFileUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                }
                allIntents.add(intent)
            }
            val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galleryIntent.type = "image/*"
            val listGallery =
                packageManager.queryIntentActivities(galleryIntent, 0)
            for (res in listGallery) {
                val intent = Intent(galleryIntent)
                intent.component =
                    ComponentName(res.activityInfo.packageName, res.activityInfo.name)
                intent.setPackage(res.activityInfo.packageName)
                allIntents.add(intent)
            }
            var mainIntent = allIntents[allIntents.size - 1]
            for (intent in allIntents) {
                if (intent.component?.getClassName() == "com.android.documentsui.DocumentsActivity") {
                    mainIntent = intent
                    break
                }
            }
            allIntents.remove(mainIntent)
            val chooserIntent = Intent.createChooser(mainIntent, "Select source")
            chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS,
                allIntents.toTypedArray<Parcelable>()
            )
            return chooserIntent
        }

    private val captureImageOutputUri: Uri?
        private get() {
            var outputFileUri: Uri? = null
            getImage = externalCacheDir
            Log.d("File", getImage.toString())
            if (getImage != null) {
                outputFileUri =
                    Uri.fromFile(File(getImage!!.path, "profile.png"))
            }
            return outputFileUri
        }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val bitmap: Bitmap?
        if (resultCode == Activity.RESULT_OK) {
            if (getPickImageResultUri(data) != null) {
                picUri = getPickImageResultUri(data)
                try {
                    myBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, picUri)
                    //myBitmap = rotateImageIfRequired(myBitmap, picUri);
                    myBitmap = getResizedBitmap(myBitmap, 500)
                    Log.d("Bitmap", picUri.toString())
                    croppedImageView!!.setImageBitmap(myBitmap)
                    updateProfilePic()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                bitmap = data!!.extras!!["data"] as Bitmap?
                myBitmap = bitmap
                if (croppedImageView != null) {
                    croppedImageView!!.setImageBitmap(myBitmap)
                }
            }
        }
    }

    fun getResizedBitmap(image: Bitmap?, maxSize: Int): Bitmap {
        var width = image!!.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 0) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
        }
        return if (isCamera) captureImageOutputUri else data!!.data
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("pic_uri", picUri)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        picUri = savedInstanceState.getParcelable("pic_uri")
    }

    private fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }
        return result
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= VERSION_CODES.M) {
                return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun showMessageOKCancel(
        message: String,
        okListener: DialogInterface.OnClickListener
    ) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > VERSION_CODES.LOLLIPOP_MR1
    }

    @TargetApi(VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            ALL_PERMISSIONS_RESULT -> {
                for (perms in permissionsToRequest!!) {
                    if (hasPermission(perms)) {
                    } else {
                        permissionsRejected.add(perms)
                    }
                }
                if (permissionsRejected.size > 0) {
                    if (Build.VERSION.SDK_INT >= VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected[0])) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                DialogInterface.OnClickListener { dialog, which ->
                                    if (Build.VERSION.SDK_INT >= VERSION_CODES.M) {
                                        requestPermissions(
                                            permissionsRejected.toTypedArray(),
                                            ALL_PERMISSIONS_RESULT
                                        )
                                    }
                                })
                            return
                        }
                    }
                }
            }
        }
    }

    private fun updateProfile() {
        val url: String = AppUrls.BASE_URL + AppUrls.EDIT_PROFILE
        val jsonObj = JSONObject()
        try {
            jsonObj.put("firstName", f_name_edt!!.text.toString())
            jsonObj.put("lastName", l_name_edt!!.text.toString())
            jsonObj.put("profilePicId", fileId)
            jsonObj.put("email", email_edt!!.text.toString())
            jsonObj.put("authority", authority)
        } catch (e: Exception) {
        }
        Log.d("JsonObj", jsonObj.toString())
        val request: JsonObjectRequest = object : JsonObjectRequest(
            Method.POST,
            url,
            jsonObj,
            Response.Listener { response ->
                Log.d("ProfileStatus", response.toString())
                try {
                    val jsonObject = JSONObject(response.toString())
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    Log.d(
                        "Status", """
     $status
     $message
     """.trimIndent()
                    )
                    if (status.equals("2000", ignoreCase = true)) {
                        GlobalCalls.showToast(message, this@EditProfileActivity)
                        val intent =
                            Intent(this@EditProfileActivity, MyAccountActivity::class.java)
                        startActivity(intent)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Log.d("ResError", e.message)
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace()
                Log.d("EROORRR", error.message)
            }) {
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> =
                    HashMap()
                headers["Authorization"] = "Bearer $accessToken"
                return headers
            }
        }
        val requestQueue = Volley.newRequestQueue(this@EditProfileActivity)
        requestQueue.add(request)
    }

    override fun onBackPressed() {
        val intent = Intent(this@EditProfileActivity, SettingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    companion object {
        private const val ALL_PERMISSIONS_RESULT = 107

        @Throws(IOException::class)
        private fun rotateImageIfRequired(img: Bitmap, selectedImage: Uri): Bitmap {
            val ei = ExifInterface(selectedImage.path)
            val orientation =
                ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            return when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(
                    img,
                    90
                )
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(
                    img,
                    180
                )
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(
                    img,
                    270
                )
                else -> img
            }
        }

        private fun rotateImage(img: Bitmap, degree: Int): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(degree.toFloat())
            val rotatedImg =
                Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
            img.recycle()
            return rotatedImg
        }
    }
}